@extends('layouts.master')

@section('title')
    <title>Layanan - Cepatcair.com</title>
@endsection
  
@section('content')
    <!-- Breadcrumb Section Begin -->
    <div class="breadcrumb-option set-bg" data-setbg="{{ asset('tema/img/breadcrumb/breadcrumb-bg.jpg') }}">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="breadcrumb__text">
                        <h2>Layanan</h2>
                        <div class="breadcrumb__links">
                            <a href="{{ route('home') }}">Beranda</a>
                            <span>Layanan</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Breadcrumb Section End -->
     <!-- Services Section Begin -->
    <section class="services spad">
        <div class="container">
            <div class="row">
                @foreach($layanan as $services)
                    <div class="col-lg-4 col-md-4 col-sm-6">
                        <div class="services__item">
                            <div class="services__item__img">
                                <img src="{{ asset('uploads/loan_type/'.$services->image_front) }}" alt="">
                            </div>
                            <div class="services__item__text">
                                <h4><span>0{{ $services->no_order }}.</span> {{ $services->name }}</h4>
                                <p>{{ $services->description }}</p>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
    <!-- Services End -->

    <!-- Contact Begin -->
    <div class="contact-widget set-bg" data-setbg="{{ asset('tema/img/contact-widget.jpg') }}">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="contact__widget__item">
                                <h4>Alamat Kami</h4>
                                <p>{{ $footer->alamat }}</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="contact__widget__phone">
                        <span>Hubungi Kami Sekarang!</span>
                        <h2>{{ $footer->no_telp }}</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Contact End -->
@endsection