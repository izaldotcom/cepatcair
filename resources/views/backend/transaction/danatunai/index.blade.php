  @extends('backend.layouts.master')

  @section('title')
      <title>List Transaksi</title>
  @endsection
  
  @section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">List Transaksi</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('backend.home') }}">Home</a></li>
              <li class="breadcrumb-item active">Callback</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                  @include ('partials.messages')
                  <div class="table-responsive" style="padding-top: 10px;">
                    <table class="table table-hover" style="width:100%" id="table1">
                        <thead>
                            <tr align="center">
                                <td>Aksi</td>
                                <td>Nama</td>
                                <td>No Telepon</td>
                                <td>Email</td>
                                <td>Tipe Jaminan</td>
                                <td>Pemilik BPKB / Sertifikat</td>
                                <td>Alamat</td>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                  </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
      <h5>Title</h5>
      <p>Sidebar content</p>
    </div>
  </aside>
  <!-- /.control-sidebar -->
  @endsection
  @section('scripts')
  @parent
  <script type="text/javascript">
      var dataTable = $('#table1').DataTable({
          processing: true,
          ajax:{
              url: "/backend/transaction/danatunai"
          },
          columns: [
              {
                  data: "action",
                  orderable: false,
                  searchable: false,
                  render: function (data, type, row) {
                      var id = row.idTrDanaTunai;
                      return `
                              <a href="/backend/transaction/danatunai/delete/`+id+`" class="badge bg-danger" onclick="konfirmasiHapus(event)"><i class="fa fa-trash"></i></a>
                          `;
                      // return `
                      //         <a href="/backend/transaction/danatunai/edit/`+id+`" class="badge bg-primary"><i class="fa fa-check"></i></a>
                      //         <a href="/backend/transaction/danatunai/delete/`+id+`" class="badge bg-danger" onclick="konfirmasiHapus(event)"><i class="fa fa-trash"></i></a>
                      //     `;
                  }
              },
              { data:'nama_lengkap' },
              { data:'no_telp' },
              { data:'email' },
              { data:'tipe' },
              { data:'pemilik_jaminan' },
              { data:'alamat' },
          ],
          searchDelay: 750,
          buttons: [],
          columnDefs: [
              {
                  targets: [ 0, 1, 2, 3, 4, 5, 6],
                  className: "text-center"
              },
          ],
      });
  </script>
  @endsection