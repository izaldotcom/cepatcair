  @extends('backend.layouts.master')

  @section('title')
      <title>Edit SubMenu Front</title>
  @endsection
  
  @section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Edit SubMenu</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('backend.home') }}">Home</a></li>
              <li class="breadcrumb-item"><a href="{{ route('submenufront.index') }}">Menu</a></li>
              <li class="breadcrumb-item active">Edit SubMenu</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
          <div class="row">
              <div class="col-md-12">
                <form action="{{ route('submenufront.update', $submenu->id) }}" method="post">
                    @csrf
                    <input type="hidden" name="_method" value="PUT">
                    <div class="form-group">
                        <label for="">Menu</label>
                        <select name="menu_id" id="menu_id" 
                            required class="form-control {{ $errors->has('menu_id') ? 'is-invalid':'' }}">
                            <option value="">Pilih</option>
                            @foreach ($menu as $row)
                                <option value="{{ $row->id }}" {{ $row->id == $submenu->menu_id ? 'selected':'' }}>
                                    {{ ucfirst($row->name) }}
                                </option>
                            @endforeach
                        </select>
                        <p class="text-danger">{{ $errors->first('menu_id') }}</p>
                    </div>
                    <div class="form-group">
                        <label for="">Nama</label>
                        <input type="text" name="name" 
                            value="{{ $submenu->name }}"
                            class="form-control {{ $errors->has('name') ? 'is-invalid':'' }}">
                        <p class="text-danger">{{ $errors->first('name') }}</p>
                    </div>
                    <div class="form-group">
                        <label for="">Url</label>
                        <input type="text" name="url" 
                            value="{{ $submenu->url }}"
                            class="form-control {{ $errors->has('url') ? 'is-invalid':'' }}">
                        <p class="text-danger">{{ $errors->first('url') }}</p>
                    </div>
                    <div class="form-group">
                        <label for="">Status</label>
                        <select name="status" id="status" 
                            required class="form-control {{ $errors->has('status') ? 'is-invalid':'' }}">
                            <option value="0" {{ $submenu->status == '0' ? 'selected':'' }}>Tidak Aktif</option>
                            <option value="1" {{ $submenu->status == '1' ? 'selected':'' }}>Aktif</option>
                        </select>
                        <p class="text-danger">{{ $errors->first('status') }}</p>
                    </div>
                     <div class="form-group">
                        <label for="">Urutan SubMenu</label>
                        <input type="number" name="no_order" 
                            value="{{ $submenu->no_order }}"
                            class="form-control {{ $errors->has('no_order') ? 'is-invalid':'' }}">
                        <p class="text-danger">{{ $errors->first('no_order') }}</p>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-success btn-sm">
                            <i class="fa fa-send"></i> Simpan
                        </button>
                        <a href="{{ url()->previous() }}" class="btn btn-primary btn-sm">Kembali</a>
                    </div>
                </form>
              </div>
          </div>
      </div>
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
      <h5>Title</h5>
      <p>Sidebar content</p>
    </div>
  </aside>
  <!-- /.control-sidebar -->
  @endsection