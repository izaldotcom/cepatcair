@extends('backend.layouts.master')

@section('title')
    <title>Landing Page</title>
@endsection

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Landing Page</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{ route('backend.home') }}">Home</a></li>
            <li class="breadcrumb-item"><a href="{{ route('landingpage.index') }}">Landing Page</a></li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <div class="content">
    <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <form action="{{ route('landingpage.update', $data->id) }}" method="post" enctype="multipart/form-data">
                @csrf
                @include ('partials.messages')
                <input type="hidden" name="_method" value="PUT">
                <div class="form-group">
                      <label for="">Banner</label>
                      <input type="file" name="banner" class="form-control">
                      <p class="text-danger">{{ $errors->first('banner') }}</p>
                      @if (!empty($data->banner))
                          <hr>
                          <img src="{{ asset('uploads/landing_page/' . $data->banner) }}" 
                              alt=""
                              width="150px" height="150px">
                      @endif
                  </div>
                <div class="form-group">
                    <label for="">Teks Pertama</label>
                    <input type="text" name="teks_1" value="{{ $data->teks_1 }}" class="form-control {{ $errors->has('teks_1') ? 'is-invalid':'' }}">
                    <p class="text-danger">{{ $errors->first('teks_1') }}</p>
                </div>
                <div class="form-group">
                    <label for="">Deskripsi Pertama</label>
                    <input type="text" name="deskripsi_1" value="{{ $data->deskripsi_1 }}" class="form-control {{ $errors->has('deskripsi_1') ? 'is-invalid':'' }}">
                    <p class="text-danger">{{ $errors->first('deskripsi_1') }}</p>
                </div>
                <div class="form-group">
                    <label for="">Teks Kedua</label>
                    <input type="text" name="teks_2" value="{{ $data->teks_2 }}" class="form-control {{ $errors->has('teks_2') ? 'is-invalid':'' }}">
                    <p class="text-danger">{{ $errors->first('teks_2') }}</p>
                </div>
                <div class="form-group">
                    <label for="">Deskripsi Kedua</label>
                    <input type="text" name="deskripsi_2" value="{{ $data->deskripsi_2 }}" class="form-control {{ $errors->has('deskripsi_2') ? 'is-invalid':'' }}">
                    <p class="text-danger">{{ $errors->first('deskripsi_2') }}</p>
                </div>
                <div class="form-group">
                    <label for="">Teks Ketiga</label>
                    <input type="text" name="teks_3" value="{{ $data->teks_3 }}" class="form-control {{ $errors->has('teks_3') ? 'is-invalid':'' }}">
                    <p class="text-danger">{{ $errors->first('teks_3') }}</p>
                </div>
                <div class="form-group">
                    <label for="">Deskripsi Ketiga</label>
                    <input type="text" name="deskripsi_3" value="{{ $data->deskripsi_3 }}" class="form-control {{ $errors->has('deskripsi_3') ? 'is-invalid':'' }}">
                    <p class="text-danger">{{ $errors->first('deskripsi_3') }}</p>
                </div>
                <div class="form-group">
                    <button class="btn btn-success btn-sm">
                        <i class="fa fa-send"></i> Simpan
                    </button>
                    <a href="/backend/home" class="btn btn-primary btn-sm">Kembali</a>
                </div>
            </form>
          </div>
        </div>
    </div>
  </div>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
  <!-- Control sidebar content goes here -->
  <div class="p-3">
    <h5>Title</h5>
    <p>Sidebar content</p>
  </div>
</aside>
<!-- /.control-sidebar -->
@endsection