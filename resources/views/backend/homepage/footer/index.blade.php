  @extends('backend.layouts.master')

  @section('title')
      <title>Data Footer</title>
  @endsection
  
  @section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Data Footer</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('backend.home') }}">Home</a></li>
              <li class="breadcrumb-item"><a href="{{ route('footer.index') }}">Footer</a></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <form action="{{ route('footer.update', $data->id) }}" method="post" enctype="multipart/form-data">
                  @csrf
                  @include ('partials.messages')
                  <input type="hidden" name="_method" value="PUT">
                  <div class="form-group">
                      <label for="">Alamat</label>
                      <input type="text" name="alamat" value="{{ $data->alamat }}" class="form-control {{ $errors->has('alamat') ? 'is-invalid':'' }}">
                      <p class="text-danger">{{ $errors->first('alamat') }}</p>
                  </div>
                  <div class="form-group">
                      <label for="">No Telp</label>
                      <input type="text" name="no_telp" value="{{ $data->no_telp }}" class="form-control {{ $errors->has('no_telp') ? 'is-invalid':'' }}">
                      <p class="text-danger">{{ $errors->first('no_telp') }}</p>
                  </div>
                  <div class="form-group">
                      <label for="">Email</label>
                      <input type="email" name="email" value="{{ $data->email }}" class="form-control {{ $errors->has('email') ? 'is-invalid':'' }}">
                      <p class="text-danger">{{ $errors->first('email') }}</p>
                  </div>
                  <div class="form-group">
                      <label for="">Jam Operasional</label>
                      <input type="text" name="jam_operasional" value="{{ $data->jam_operasional }}" class="form-control {{ $errors->has('jam_operasional') ? 'is-invalid':'' }}">
                      <p class="text-danger">{{ $errors->first('jam_operasional') }}</p>
                  </div>
                  <div class="form-group">
                      <label for="">Facebook</label>
                      <input type="text" name="url_fb" value="{{ $data->url_fb }}" class="form-control {{ $errors->has('url_fb') ? 'is-invalid':'' }}">
                      <p class="text-danger">{{ $errors->first('url_fb') }}</p>
                  </div>
                  <div class="form-group">
                      <label for="">Twitter</label>
                      <input type="text" name="url_twitter" value="{{ $data->url_twitter }}" class="form-control {{ $errors->has('url_twitter') ? 'is-invalid':'' }}">
                      <p class="text-danger">{{ $errors->first('url_twitter') }}</p>
                  </div>
                  <div class="form-group">
                      <label for="">Instagram</label>
                      <input type="text" name="url_ig" value="{{ $data->url_ig }}" class="form-control {{ $errors->has('url_ig') ? 'is-invalid':'' }}">
                      <p class="text-danger">{{ $errors->first('url_ig') }}</p>
                  </div>
                  <div class="form-group">
                      <label for="">Deskripsi</label>
                      <input type="text" name="deskripsi" value="{{ $data->deskripsi }}" class="form-control {{ $errors->has('deskripsi') ? 'is-invalid':'' }}">
                      <p class="text-danger">{{ $errors->first('deskripsi') }}</p>
                  </div>
                  <div class="form-group">
                      <button class="btn btn-success btn-sm">
                          <i class="fa fa-send"></i> Simpan
                      </button>
                      <a href="/backend/home" class="btn btn-primary btn-sm">Kembali</a>
                  </div>
              </form>
            </div>
          </div>
      </div>
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
      <h5>Title</h5>
      <p>Sidebar content</p>
    </div>
  </aside>
  <!-- /.control-sidebar -->
  @endsection