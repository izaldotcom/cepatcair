  @extends('backend.layouts.master')

  @section('title')
      <title>Data About Us</title>
  @endsection
  
  @section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Data About Us</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('backend.home') }}">Home</a></li>
              <li class="breadcrumb-item"><a href="{{ route('aboutus.index') }}">About Us</a></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <form action="{{ route('aboutus.update', $data->id) }}" method="post" enctype="multipart/form-data">
                  @csrf
                  @include ('partials.messages')
                  <input type="hidden" name="_method" value="PUT">
                  <div class="form-group">
                      <label for="">Judul</label>
                      <input type="text" name="judul" value="{{ $data->judul }}" class="form-control {{ $errors->has('judul') ? 'is-invalid':'' }}">
                      <p class="text-danger">{{ $errors->first('judul') }}</p>
                  </div>
                  <div class="form-group">
                      <label for="">Pengantar</label>
                      <textarea class="form-control {{ $errors->has('pengantar') ? 'is-invalid':'' }}" name="pengantar" rows="3">{{ $data->pengantar }}</textarea>
                      <p class="text-danger">{{ $errors->first('pengantar') }}</p>
                  </div>
                  <div class="form-group">
                      <label for="">Isi</label>
                      <textarea class="form-control {{ $errors->has('isi') ? 'is-invalid':'' }}" name="isi" rows="3">{{ $data->isi }}</textarea>
                      <p class="text-danger">{{ $errors->first('isi') }}</p>
                  </div>
                  <div class="form-group">
                      <label for="">Judul Visi</label>
                      <input type="text" name="judul_visi" value="{{ $data->judul_visi }}" class="form-control {{ $errors->has('judul_visi') ? 'is-invalid':'' }}">
                      <p class="text-danger">{{ $errors->first('judul_visi') }}</p>
                  </div>
                  <div class="form-group">
                      <label for="">Isi Visi</label>
                      <textarea class="form-control {{ $errors->has('isi_visi') ? 'is-invalid':'' }}" name="isi_visi" rows="3">{{ $data->isi_visi }}</textarea>
                      <p class="text-danger">{{ $errors->first('isi_visi') }}</p>
                  </div>
                  <div class="form-group">
                      <label for="">Judul Misi</label>
                      <input type="text" name="judul_misi" value="{{ $data->judul_misi }}" class="form-control {{ $errors->has('judul_misi') ? 'is-invalid':'' }}">
                      <p class="text-danger">{{ $errors->first('judul_misi') }}</p>
                  </div>
                  <div class="form-group">
                      <label for="">Isi Misi</label>
                      <textarea class="form-control {{ $errors->has('isi_misi') ? 'is-invalid':'' }}" name="isi_misi" rows="3">{{ $data->isi_misi }}</textarea>
                      <p class="text-danger">{{ $errors->first('isi_misi') }}</p>
                  </div>
                  <div class="form-group">
                      <label for="">Judul Value</label>
                      <input type="text" name="judul_value" value="{{ $data->judul_value }}" class="form-control {{ $errors->has('judul_value') ? 'is-invalid':'' }}">
                      <p class="text-danger">{{ $errors->first('judul_value') }}</p>
                  </div>
                  <div class="form-group">
                      <label for="">Isi Value</label>
                      <textarea class="form-control {{ $errors->has('isi_value') ? 'is-invalid':'' }}" name="isi_value" rows="3">{{ $data->isi_value }}</textarea>
                      <p class="text-danger">{{ $errors->first('isi_value') }}</p>
                  </div>
                  <div class="form-group">
                      <label for="">Url Video</label>
                      <textarea class="form-control {{ $errors->has('url_video') ? 'is-invalid':'' }}" name="url_video" rows="3">{{ $data->url_video   }}</textarea>
                      <p class="text-danger">{{ $errors->first('url_video') }}</p>
                  </div>
                  <div class="form-group">
                      <button class="btn btn-success btn-sm">
                          <i class="fa fa-send"></i> Simpan
                      </button>
                      <a href="/backend/home" class="btn btn-primary btn-sm">Kembali</a>
                  </div>
              </form>
            </div>
          </div>
      </div>
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
      <h5>Title</h5>
      <p>Sidebar content</p>
    </div>
  </aside>
  <!-- /.control-sidebar -->
  @endsection