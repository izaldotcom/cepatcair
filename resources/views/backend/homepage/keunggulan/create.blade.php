@extends('backend.layouts.master')

@section('title')
    <title>Tambah Data</title>
@endsection

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Tambah Data</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{ route('backend.home') }}">Home</a></li>
            <li class="breadcrumb-item"><a href="{{ route('keunggulan.index') }}">Keunggulan</a></li>
            <li class="breadcrumb-item active">Tambah</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <div class="content">
    <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <form action="{{ route('keunggulan.store') }}" method="post" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="id" id="id">
                <div class="form-group">
                    <label for="">Ikon</label>
                    <input type="file" name="icon" class="form-control">
                    <p class="text-danger">{{ $errors->first('icon') }}</p>
                </div>
                <div class="form-group">
                    <label for="">Judul</label>
                    <input type="text" name="title" class="form-control {{ $errors->has('title') ? 'is-invalid':'' }}">
                    <p class="text-danger">{{ $errors->first('title') }}</p>
                </div>
                <div class="form-group">
                    <label for="">Deskripsi</label>
                    <textarea class="form-control {{ $errors->has('description') ? 'is-invalid':'' }}" name="description" rows="3"></textarea>
                    <p class="text-danger">{{ $errors->first('description') }}</p>
                </div>
                <div class="form-group">
                    <button class="btn btn-success btn-sm">
                        <i class="fa fa-send"></i> Simpan
                    </button>
                    <a href="{{ url()->previous() }}" class="btn btn-primary btn-sm">Kembali</a>
                </div>
            </form>
          </div>
        </div>
    </div>
  </div>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
  <!-- Control sidebar content goes here -->
  <div class="p-3">
    <h5>Title</h5>
    <p>Sidebar content</p>
  </div>
</aside>
<!-- /.control-sidebar -->
@endsection