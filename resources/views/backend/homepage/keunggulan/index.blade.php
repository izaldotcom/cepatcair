  @extends('backend.layouts.master')

  @section('title')
      <title>List Keunggulan</title>
  @endsection
  
  @section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Data Keunggulan</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('backend.home') }}">Home</a></li>
              <li class="breadcrumb-item active">Keunggulan</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                  <a href="{{ route('keunggulan.create') }}" class="btn btn-primary btn-sm">Tambah Baru</a>
                  @include ('partials.messages')
                  <div class="table-responsive" style="padding-top: 10px;">
                    <table class="table table-hover" style="width:100%" id="table1">
                        <thead>
                            <tr align="center">
                                <td>Aksi</td>
                                <td>Ikon</td>
                                <td>Judul</td>
                                <td>Deskripsi</td>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                  </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
      <h5>Title</h5>
      <p>Sidebar content</p>
    </div>
  </aside>
  <!-- /.control-sidebar -->
  @endsection
  @section('scripts')
  @parent
  <script type="text/javascript">
      var dataTable = $('#table1').DataTable({
          processing: true,
          ajax:{
              url: "/backend/homepage/keunggulan"
          },
          columns: [
              {
                  data: "action",
                  orderable: false,
                  searchable: false,
                  render: function (data, type, row) {
                      var id = row.idKeunggulan;
                      return `
                              <a href="/backend/homepage/keunggulan/edit/`+id+`" class="badge bg-primary"><i class="fa fa-check"></i></a>
                              <a href="/backend/homepage/keunggulan/delete/`+id+`" class="badge bg-danger" onclick="konfirmasiHapus(event)"><i class="fa fa-trash"></i></a>
                          `;
                  }
              },
              { data:'ikon' },
              { data:'title' },
              { data:'description' },
          ],
          searchDelay: 750,
          buttons: [],
          columnDefs: [
              {
                  targets: [ 0, 1, 2, 3 ],
                  className: "text-center"
              },
          ],
      });
  </script>
  @endsection