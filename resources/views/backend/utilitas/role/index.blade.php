 @extends('backend.layouts.master')

  @section('title')
      <title>Manajemen Role</title>
  @endsection
  
  @section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Manajemen Role</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('backend.home') }}">Home</a></li>
              <li class="breadcrumb-item active">Role</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            @include ('partials.messages')
            <div class="row">
              <div class="col-md-4">
                <h4 class="card-title">Add Role</h4><br>
                <form role="form" action="{{ route('role.store') }}" method="POST">
                    @csrf
                    <div class="form-group" style="padding-top: 15px;">
                        <label for="name">Role</label>
                        <input type="text" 
                        name="name"
                        class="form-control {{ $errors->has('name') ? 'is-invalid':'' }}" id="name" required>
                    </div>
                    <div class="card-footer">
                        <button class="btn btn-success">Simpan</button>
                        <a href="{{ route('home') }}" class="btn btn-primary">Kembali</a>
                    </div>
                </form>
              </div>
              <div class="col-md-8">
                <h4 class="card-title">List Role</h4><br>
                <div class="table-responsive" style="padding-top: 10px;">
                    <table class="table table-hover" style="width:100%" id="myTable">
                        <thead>
                            <tr align="center">
                                <td>No</td>
                                <td>Role</td>
                                <td>Aksi</td>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($role as $row)
                            <tr align="center">
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $row->name }}</td>
                                <td>
                                    <form action="{{ route('role.destroy', $row->id) }}" method="POST">
                                        @csrf
                                        <input type="hidden" name="_method" value="DELETE">
                                        <button class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>
                                    </form>
                                </td>
                            </tr>
                            @empty
                            <tr align="center">
                                <td colspan="8" class="text-center">Tidak ada data</td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
                <div class="float-right">
                    {!! $role->links() !!}
                </div>
              </div>
            </div>
        </div>
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
      <h5>Title</h5>
      <p>Sidebar content</p>
    </div>
  </aside>
  <!-- /.control-sidebar -->
  @endsection