  @extends('backend.layouts.master')

  @section('title')
      <title>Tambah Menu</title>
  @endsection
  
  @section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Tambah Menu</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('backend.home') }}">Home</a></li>
              <li class="breadcrumb-item"><a href="{{ route('menu.index') }}">Menu</a></li>
              <li class="breadcrumb-item active">Tambah</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <form action="{{ route('menu.store') }}" method="post">
                  @csrf
                  <input type="hidden" name="id" id="id">
                  <div class="form-group">
                      <label for="">Nama</label>
                      <input type="text" name="name" class="form-control {{ $errors->has('name') ? 'is-invalid':'' }}">
                      <p class="text-danger">{{ $errors->first('name') }}</p>
                  </div>
                  <div class="form-group">
                      <label for="">Ikon</label>
                      <input type="text" name="icon" class="form-control {{ $errors->has('icon') ? 'is-invalid':'' }}">
                      <p class="text-danger">{{ $errors->first('icon') }}</p>
                  </div>
                  <div class="form-group">
                      <label for="">Url</label>
                      <input type="text" name="url" class="form-control {{ $errors->has('url') ? 'is-invalid':'' }}">
                      <p class="text-danger">{{ $errors->first('url') }}</p>
                  </div>
                  <div class="form-group">
                      <label for="">Deskripsi</label>
                      <input type="text" name="description" class="form-control {{ $errors->has('description') ? 'is-invalid':'' }}">
                      <p class="text-danger">{{ $errors->first('description') }}</p>
                  </div>
                  <div class="form-group">
                      <label for="">Bercabang</label>
                      <select name="is_parent" id="is_parent" 
                          required class="form-control {{ $errors->has('is_parent') ? 'is-invalid':'' }}">
                          <option value="0">Tidak</option>
                          <option value="1">Ya</option>
                      </select>
                      <p class="text-danger">{{ $errors->first('is_parent') }}</p>
                  </div>
                  <div class="form-group">
                      <label for="">Urutan Menu</label>
                      <input type="number" name="no_order" class="form-control {{ $errors->has('no_order') ? 'is-invalid':'' }}">
                      <p class="text-danger">{{ $errors->first('no_order') }}</p>
                  </div>
                  <div class="form-group">
                      <button class="btn btn-success btn-sm">
                          <i class="fa fa-send"></i> Simpan
                      </button>
                      <a href="{{ url()->previous() }}" class="btn btn-primary btn-sm">Kembali</a>
                  </div>
              </form>
            </div>
          </div>
      </div>
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
      <h5>Title</h5>
      <p>Sidebar content</p>
    </div>
  </aside>
  <!-- /.control-sidebar -->
  @endsection