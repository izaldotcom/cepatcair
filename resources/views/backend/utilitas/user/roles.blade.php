  @extends('backend.layouts.master')

  @section('title')
      <title>User Roles</title>
  @endsection
  
  @section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">User Roles</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('backend.home') }}">Home</a></li>
              <li class="breadcrumb-item"><a href="{{ route('user.index') }}">User</a></li>
              <li class="breadcrumb-item active">User Roles</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
          <div class="row">
              <div class="col-md-6">
                  <form action="{{ route('user.set_role', $user->id) }}" method="post">
                      @csrf
                      <input type="hidden" name="_method" value="PUT">

                      @include ('partials.messages')
                      
                      <div class="table-responsive">
                          <table class="table table-hover table-bordered">
                              <thead>
                                  <tr>
                                      <th>Nama</th>
                                      <td>:</td>
                                      <td>{{ $user->name }}</td>
                                  </tr>
                                  <tr>
                                      <th>Email</th>
                                      <td>:</td>
                                      <td><a href="mailto:{{ $user->email }}">{{ $user->email }}</a></td>
                                  </tr>
                                  <tr>
                                      <th>Role</th>
                                      <td>:</td>
                                      <td>
                                          @foreach ($roles as $row)
                                          <input type="radio" name="role" 
                                              {{ $user->hasRole($row) ? 'checked':'' }}
                                              value="{{ $row }}"> {{  $row }} <br>
                                          @endforeach
                                      </td>
                                  </tr>
                              </thead>
                          </table>
                      </div>
                      <div class="card-footer">
                          <button type="submit" class="btn btn-success btn-sm">
                              Set Role
                          </button>
                          <a href="{{ route('user.index') }}" class="btn btn-primary btn-sm">Kembali</a>
                      </div>
                  </form>
              </div>
          </div>
      </div>
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
      <h5>Title</h5>
      <p>Sidebar content</p>
    </div>
  </aside>
  <!-- /.control-sidebar -->
  @endsection