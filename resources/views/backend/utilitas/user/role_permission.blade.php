  @extends('backend.layouts.master')

  @section('title')
      <title>Permission</title>
  @endsection
  
  @section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Permission</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('backend.home') }}">Home</a></li>
              <li class="breadcrumb-item active">Permission</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
            @include ('partials.messages')
            <div class="row">
                <div class="col-md-4">
                    <h4 class="card-title">Add New Permission</h4><br>
                    <form action="{{ route('user.add_permission') }}" method="post">
                        @csrf
                        <div class="form-group">
                            <label for="">List Sub Menu</label>
                            <select name="name" id="name" 
                                required class="form-control {{ $errors->has('name') ? 'is-invalid':'' }}">
                                @foreach ($submenu as $row)
                                    <option value="{{ str_replace(' ', '', strtolower($row->menu->name)) }}-{{ str_replace(' ', '', strtolower($row->name)) }}">{{ $row->menu->name }} - {{ ucfirst($row->name) }}</option>
                                @endforeach
                            </select>
                            <!-- <input type="text" name="name" class="form-control {{ $errors->has('name') ? 'is-invalid':'' }}" required> -->
                            <p class="text-danger">{{ $errors->first('name') }}</p>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-success btn-sm">
                                <i class="fa fa-send"></i> Simpan
                            </button>
                            <a href="{{ route('home') }}" class="btn btn-primary btn-sm">Kembali</a>
                        </div>
                    </form>
                </div>
    ​
                <div class="col-md-8">
                     ​<h4 class="card-title">Set Permission to Role</h4><br>
                     @include ('partials.messages')
                        <form action="{{ route('user.roles_permission') }}" method="GET">
                            <div class="form-group">
                                <label for="">Roles</label>
                                <div class="input-group">
                                    <select name="role" class="form-control">
                                        @foreach ($roles as $value)
                                            <option value="{{ $value }}" {{ request()->get('role') == $value ? 'selected':'' }}>{{ $value }}</option>
                                        @endforeach
                                    </select>
                                    <span class="input-group-btn">
                                        <button class="btn btn-danger">Check!</button>
                                    </span>
                                </div>
                            </div>
                        </form>
                    @if (!empty($permissions))
                        <form action="{{ route('user.setRolePermission', request()->get('role')) }}" method="post">
                            @csrf
                            <input type="hidden" name="_method" value="PUT">
                            <div class="form-group">
                                <div class="nav-tabs-custom">
                                    <ul class="nav nav-tabs">
                                        <li class="active">
                                            <a href="#tab_1" data-toggle="tab">Sub Menu Set For Permission</a>
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="tab_1">
                                            @php $no = 1; @endphp
                                            @foreach ($permissions as $key => $row)
                                                <input type="checkbox" 
                                                    name="permission[]" 
                                                    class="minimal-red" 
                                                    value="{{ $row }}"
                                                    {{--  CHECK, JIKA PERMISSION TERSEBUT SUDAH DI SET, MAKA CHECKED --}}
                                                    {{ in_array($row, $hasPermission) ? 'checked':'' }}
                                                    > {{ $row }} <br>
                                               <!--  @if ($no++%count($submenu) == 0)
                                                <br>
                                                @endif -->
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="pull-right">
                                <button class="btn btn-primary btn-sm">
                                    <i class="fa fa-send"></i> Set Permission
                                </button>
                            </div>
                        </form>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
      <h5>Title</h5>
      <p>Sidebar content</p>
    </div>
  </aside>
  <!-- /.control-sidebar -->
  @endsection