@extends('backend.layouts.master')

@section('title')
    <title>Edit User</title>
@endsection

@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Edit User</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ route('backend.home') }}">Home</a></li>
              <li class="breadcrumb-item"><a href="{{ route('user.index') }}">User</a></li>
              <li class="breadcrumb-item active">Edit User</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
          <div class="row">
              <div class="col-md-12">
                <form action="{{ route('user.update', $user->id) }}" method="post">
                    @csrf
                    <input type="hidden" name="_method" value="PUT">
                    <div class="form-group">
                        <label for="">Nama</label>
                        <input type="text" name="name" 
                            value="{{ $user->name }}"
                            class="form-control {{ $errors->has('name') ? 'is-invalid':'' }}" required>
                        <p class="text-danger">{{ $errors->first('name') }}</p>
                    </div>
                    <div class="form-group">
                        <label for="">Email</label>
                        <input type="text" name="email" 
                            value="{{ $user->email }}"
                            class="form-control {{ $errors->has('email') ? 'is-invalid':'' }}" 
                            required readonly>
                        <p class="text-danger">{{ $errors->first('email') }}</p>
                    </div>
                    <div class="form-group">
                        <label for="">Password</label>
                        <input type="password" name="password" 
                            class="form-control {{ $errors->has('password') ? 'is-invalid':'' }}">
                        <p class="text-danger">{{ $errors->first('password') }}</p>
                        <p class="text-warning">Biarkan kosong, jika tidak ingin mengganti password</p>
                    </div>
                    <div class="form-group">
                        <label for="">Status</label>
                        <select name="status" class="form-control {{ $errors->has('status') ? 'is-invalid':'' }}">
                            <option value="aktif" {{ $user->status == 'aktif' ? 'selected' : '' }}>Aktif</option>
                            <option value="suspend" {{ $user->status == 'suspend' ? 'selected' : '' }}>Suspend</option>
                        </select>
                        <p class="text-danger">{{ $errors->first('status') }}</p>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-success btn-sm">
                            <i class="fa fa-send"></i> Simpan
                        </button>
                        <a href="{{ url()->previous() }}" class="btn btn-primary btn-sm">Kembali</a>
                    </div>
                </form>
              </div>
          </div>
      </div>
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
      <h5>Title</h5>
      <p>Sidebar content</p>
    </div>
  </aside>
  <!-- /.control-sidebar -->
@endsection