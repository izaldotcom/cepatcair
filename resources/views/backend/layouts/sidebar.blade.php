<!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="/" class="brand-link">
      <img src="{{ asset('backend/dist/img/AdminLTELogo.png') }}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">CepatCair</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{ asset('uploads/admin.png') }}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="{{ route('user.edit', \Crypt::encrypt(Auth::user()->id)) }}" class="d-block">{{ Auth::user()->name }}<br>
            <i><span class="badge badge-info">{{ preg_replace('/[^A-Za-z0-9\  ]/', '', Auth::user()->roles->pluck('name')) }}</span></i></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item menu-open">
            <a href="/backend/home" class="nav-link active">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          @foreach(session('sess_menu') as $menu)
            @foreach(session('sess_usermenu') as $userMenu)
              @if($userMenu->menu_name == str_replace(' ', '', strtolower($menu->name)))
              <li class="nav-item has-treeview">
                  <a href="{{ $menu->url }}" class="nav-link">
                      <i class="nav-icon fas {{ $menu->icon }}"></i>
                      <p>
                          {{ $menu->name }}
                          @if($menu->is_parent == 1)
                          <i class="right fa fa-angle-left"></i>
                          @endif
                      </p>
                  </a>
                  @foreach(session('sess_submenu') as $submenu)
                    @if($submenu->menu_id == $menu->id)
                      @if(auth()->user()->can(str_replace(' ', '', strtolower($submenu->menu->name)).'-'.str_replace(' ', '', strtolower($submenu->name))))
                      <ul class="nav nav-treeview">
                          <li class="nav-item">
                              <a href="{{ $submenu->url }}" class="nav-link">
                                  <i class="far {{ $submenu->icon }} nav-icon"></i>
                                  <p>{{ $submenu->name }}</p>
                              </a>
                          </li>
                      </ul>
                      @endif
                    @endif
                  @endforeach
              </li>
              @endif
            @endforeach
          @endforeach
          @if( auth()->user()->hasRole('superadmin'))
          <li class="nav-item has-treeview">
              <a href="#" class="nav-link">
                  <i class="nav-icon fas fa-cogs"></i>
                  <p>
                      Utilitas
                      <i class="right fa fa-angle-left"></i>
                  </p>
              </a>
              <ul class="nav nav-treeview">
                  <li class="nav-item">
                      <a href="{{ route('user.index') }}" class="nav-link">
                          <i class="far fa-circle nav-icon"></i>
                          <p>User</p>
                      </a>
                  </li>
                  <li class="nav-item">
                      <a href="{{ route('role.index') }}" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                          <p>Role</p>
                      </a>
                  </li>
                  <li class="nav-item">
                      <a href="{{ route('user.roles_permission') }}" class="nav-link">
                          <i class="far fa-circle nav-icon"></i>
                          <p>Permission</p>
                      </a>
                  </li>
              </ul>
              <ul class="nav nav-treeview">
                  <li class="nav-item">
                      <a href="{{ route('menu.index') }}" class="nav-link">
                          <i class="far fa-circle nav-icon"></i>
                          <p>Menu</p>
                      </a>
                  </li>
              </ul>
              <ul class="nav nav-treeview">
                  <li class="nav-item">
                      <a href="{{ route('submenu.index') }}" class="nav-link">
                          <i class="far fa-circle nav-icon"></i>
                          <p>Sub Menu</p>
                      </a>
                  </li>
              </ul>
          </li>
          @endif
          <li class="nav-item">
            <a class="nav-link" href="{{ route('logout-system') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
              <i class="nav-icon fas fa-sign-out-alt"></i><p>{{ __('Logout') }}</p>
            </a>     ​
            <form id="logout-form" action="{{ route('logout-system') }}" method="POST" style="display: none;">
                @csrf
            </form>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>