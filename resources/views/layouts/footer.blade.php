    <!-- Footer Section Begin -->
    <footer class="footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="footer__about">
                        <div class="footer__logo">
                            <a href="./index.html"><img src="{{ asset('tema/img/footer-logo.png') }}" alt=""></a>
                        </div>
                        <p>{{ $footer->deskripsi }}</p>
                    </div>
                </div>
                <div class="col-lg-2 offset-lg-1 col-md-3 col-sm-6">
                    <div class="footer__widget">
                        <h5>Layanan</h5>
                        <div class="footer__widget">
                            <ul>
                                @foreach($loanTypeFooter as $typeLoan)
                                <li><a href="{{ $typeLoan->url }}">{{ $typeLoan->name }}</a></li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-6">
                    <div class="footer__widget">
                        <h5>Sosial Media</h5>
                        <div class="footer__widget footer__widget--social">
                            <ul>
                                <li><a href="{{ $footer->url_fb }}"><i class="fa fa-facebook"></i> Facebook</a></li>
                                <li><a href="{{ $footer->url_ig }}"><i class="fa fa-twitter"></i> Twitter</a></li>
                                <li><a href="{{ $footer->url_twitter }}"><i class="fa fa-instagram"></i> Instagram</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 offset-lg-1 col-md-6 col-sm-6">
                    <div class="footer__widget footer__widget--address">
                        <h5>Jam Operasional</h5>
                        <ul>
                            <li>{{ $footer->jam_operasional }}</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="footer__copyright">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        <div class="footer__copyright__text">
                            <p>Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>.</p>
                        </div>
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- Footer Section End -->

    <!-- Search Begin -->
    <div class="search-model">
        <div class="h-100 d-flex align-items-center justify-content-center">
            <div class="search-close-switch">+</div>
            <form class="search-model-form">
                <input type="text" id="search-input" placeholder="Search here.....">
            </form>
        </div>
    </div>
    <!-- Search End -->

    <!-- Js Plugins -->
    <script src="{{ asset('tema/js/jquery-3.3.1.min.js') }}"></script>
    <script src="{{ asset('tema/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('tema/js/jquery.nice-select.min.js') }}"></script>
    <script src="{{ asset('tema/js/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('tema/js/jquery.nicescroll.min.js') }}"></script>
    <script src="{{ asset('tema/js/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ asset('tema/js/jquery.slicknav.js') }}"></script>
    <script src="{{ asset('tema/js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('tema/js/main.js') }}"></script>
    <script src="{{ asset('tema/sweet-alert2/sweetalert2.min.js') }}"></script>
    <script src="{{ asset('tema/js/serializeObject.min.js') }}"></script>
    @yield('scripts')
</body>
</html>