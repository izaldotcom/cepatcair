<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Loanday Template">
    <meta name="keywords" content="Loanday, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @yield('title')

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Lato:wght@300;400;700;900&display=swap" rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="{{ asset('tema/css/bootstrap.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('tema/css/font-awesome.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('tema/css/elegant-icons.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('tema/css/nice-select.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('tema/css/magnific-popup.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('tema/css/jquery-ui.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('tema/css/owl.carousel.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('tema/css/slicknav.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('tema/css/style.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('tema/sweet-alert2/sweetalert2.min.css') }}" type="text/css">
</head>

<body>
    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>

    <!-- Offcanvas Menu Begin -->
    <div class="offcanvas-menu-overlay"></div>
    <div class="offcanvas-menu-wrapper">
        <div class="offcanvas__search">
            <i class="fa fa-search search-switch"></i>
        </div>
        <div class="offcanvas__logo">
            <a href="/"><img src="{{ asset('tema/img/logo.png') }}" alt=""></a>
        </div>
        <nav class="offcanvas__menu mobile-menu">
            <ul>
                <li class="active"><a href="/">Beranda</a></li>
                @foreach($menuMobile as $menuMobile)
                    @if($menuMobile->is_parent == 0)
                    <li><a href="{{ $menuMobile->url }}">{{ $menuMobile->name }}</a></li>
                    @else
                    <li><a href="#">{{ $menuMobile->name }}</a>
                        <ul class="dropdown">
                            @foreach($submenuMobile as $submenuMobile)
                                @if($submenuMobile->menu_id == $menuMobile->id)
                                <li><a href="{{ $submenuMobile->url }}">{{ $submenuMobile->name }}</a></li>
                                @endif
                            @endforeach
                        </ul>
                    </li>
                    @endif
                @endforeach
            </ul>
        </nav>
        <div id="mobile-menu-wrap"></div>
        <ul class="offcanvas__widget">
            <li><i class="fa fa-map-marker"></i> {{ $footer->alamat }}</li>
            <li><i class="fa fa-phone"></i> {{ $footer->no_telp }}</li>
            <li><i class="fa fa-envelope"></i> {{ $footer->email }}</li>
        </ul>
    </div>
    <!-- Offcanvas Menu End -->

    <!-- Header Section Begin -->
    <header class="header">
        <div class="header__top">
            <div class="container">
                <div class="row">
                    <div class="col-lg-9">
                        <ul class="header__top__widget">
                            <li><i class="fa fa-map-marker"></i> {{ $footer->alamat }}</li>
                            <li><i class="fa fa-phone"></i> {{ $footer->no_telp }}</li>
                            <li><i class="fa fa-envelope"></i> {{ $footer->email }}</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="header__logo">
                        <a href="/"><img src="{{ asset('tema/img/logo.png') }}" alt=""></a>
                    </div>
                </div>
                <div class="col-lg-9">
                    <div class="header__nav">
                        <nav class="header__menu">
                            <ul>
                                <li class="active"><a href="/">Beranda</a></li>
                                @foreach($menu as $menu)
                                    @if($menu->is_parent == 0)
                                    <li><a href="{{ $menu->url }}">{{ $menu->name }}</a></li>
                                    @else
                                    <li><a href="#">{{ $menu->name }}</a>
                                        <ul class="dropdown">
                                            @foreach($submenu as $submenu)
                                                @if($submenu->menu_id == $menu->id)
                                                <li><a href="{{ $submenu->url }}">{{ $submenu->name }}</a></li>
                                                @endif
                                            @endforeach
                                        </ul>
                                    </li>
                                    @endif
                                @endforeach
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
            <div class="canvas__open">
                <span class="fa fa-bars"></span>
            </div>
        </div>
    </header>
    <!-- Header Section End -->