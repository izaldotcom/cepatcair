@extends('layouts.master')

@section('title')
  <title>Dashboard - Cepatcair.com</title>
@endsection
  
@section('content')
    <!-- Hero Section Begin -->
    <section class="hero set-bg" data-setbg="{{ asset('uploads/landing_page/'.$landingPage->banner) }}">
        <div class="container">
            <div class="row">
                <div class="col-lg-5">
                    <div class="hero__text">
                        <h2>{{ $landingPage->teks_1 }}</h2>
                        <p>{{ $landingPage->deskripsi_1 }}</p>
                    </div>
                </div>
                <!-- <div class="col-lg-5 offset-lg-2">
                    <div class="hero__form">
                        <h3>How much do you need</h3>
                        <form action="#">
                            <div class="input-list">
                                <div class="input-list-item">
                                    <p>Amount of money ($):</p>
                                    <input type="text">
                                </div>
                                <div class="input-list-item">
                                    <p>How long for (day):</p>
                                    <input type="text">
                                </div>
                            </div>
                            <div class="input-full-width">
                                <p>Repayment:</p>
                                <input type="text">
                            </div>
                            <div class="input-list last">
                                <div class="input-list-item">
                                    <p>Name:</p>
                                    <input type="text">
                                </div>
                                <div class="input-list-item">
                                    <p>Phone:</p>
                                    <input type="text">
                                </div>
                            </div>
                            <button type="submit" class="site-btn">Get Your Loan Now!</button>
                        </form>
                    </div>
                </div> -->
            </div>
        </div>
    </section>
    <!-- Hero Section End -->

    <!-- Loan Services Section Begin -->
    <section class="loan-services spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title">
                        <h2>{{ $landingPage->teks_2 }}</h2>
                        <p>{{ $landingPage->deskripsi_2 }}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="loan__services__list">
                @foreach($loanType as $loanType)
                <div class="loan__services__item set-bg" data-setbg="{{ asset('uploads/loan_type/'.$loanType->image) }}">
                    <div class="loan__services__item__text">
                        <h4><span>0{{ $loanType->no_order }}.</span> {{ $loanType->name }}</h4>
                        <p>{{ $loanType->description }}</p>
                        <a href="{{ $loanType->url }}">Find Out More</a>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>
    <!-- Loan Services Section End -->

    <!-- Choose Section Begin -->
    <section class="choose spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title">
                        <h2>{{ $landingPage->teks_3 }}</h2>
                        <p>{{ $landingPage->deskripsi_3 }}</p>
                    </div>
                </div>
            </div>
            <div class="row">
                @foreach($keunggulan as $keunggulan)
                    <div class="col-lg-4 col-md-4 col-sm-6">
                        <div class="choose__item">
                            <img src="{{ asset('uploads/keunggulan/'.$keunggulan->icon) }}" alt="">
                            <h5>{{ $keunggulan->title }}</h5>
                            <p>{{ $keunggulan->description }}</p>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
    <!-- Choose Section End -->

    <!-- Call Section Begin -->
    <section class="call spad set-bg" data-setbg="{{ asset('tema/img/call-bg.jpg') }}">
        <div class="container">
            <div class="row">
                <div class="col-lg-5 col-md-6">
                    <div class="call__text">
                        <div class="section-title">
                            <h2>Request A Call Back</h2>
                            <p>Posters had been a very beneficial marketing tool because it had paved to deliver an
                                effective message that conveyed customer’s attention.</p>
                        </div>
                        <a href="#">Contact Us</a>
                    </div>
                </div>
                <div class="col-lg-6 offset-lg-1 col-md-6">
                    <form class="call__form" method="post" id="form-beranda">
                        <input type="hidden" id="simpanForm" value="{{ route('send-callback') }}">
                        <div class="row">
                            <div class="col-lg-6">
                                <input type="text" name="name" placeholder="Name" required>
                            </div>
                            <div class="col-lg-6">
                                <input type="email" name="email" placeholder="Email" required>
                            </div>
                            <div class="col-lg-6">
                                <input type="text" name="phone" placeholder="Phone" required>
                            </div>
                            <div class="col-lg-6">
                                <select name="service">
                                    <option value="">Pilih Layanan</option>
                                    @foreach ($layanan as $layanan)
                                        <option value="{{ $layanan->name }}">{{ ucfirst($layanan->name) }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <button type="submit" class="site-btn" id="btnSubmitForm">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <!-- Call Section End -->

    <!-- Counter Begin -->
    <div class="counter spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-6">
                    <div class="counter__item">
                        <img src="{{ asset('tema/img/counter/counter-1.png') }}" alt="">
                        <div class="counter__number">
                            <h2 class="counter-add">2100</h2>
                        </div>
                        <p>Successful Loan Approval</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6">
                    <div class="counter__item">
                        <img src="{{ asset('tema/img/counter/counter-2.png') }}" alt="">
                        <div class="counter__number">
                            <h2 class="counter-add">99</h2>
                            <span>%</span>
                        </div>
                        <p>Customer Satisfection</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6">
                    <div class="counter__item">
                        <img src="{{ asset('tema/img/counter/counter-3.png') }}" alt="">
                        <div class="counter__number">
                            <h2 class="counter-add">90</h2>
                            <span>+</span>
                        </div>
                        <p>Office National Partners</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6">
                    <div class="counter__item">
                        <img src="{{ asset('tema/img/counter/counter-4.png') }}" alt="">
                        <div class="counter__number">
                            <h2 class="counter-add">70</h2>
                            <span>+</span>
                        </div>
                        <p>Award Certificate</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Counter End -->

    <!-- Contact Begin -->
    <div class="contact-widget set-bg" data-setbg="{{ asset('tema/img/contact-widget.jpg') }}">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="contact__widget__item">
                                <h4>Alamat Kami</h4>
                                <p>{{ $footer->alamat }}</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="contact__widget__phone">
                        <span>Hubungi Kami Sekarang!</span>
                        <h2>{{ $footer->no_telp }}</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Contact End -->
@endsection
@section('scripts')
@parent
<script type="text/javascript">
    
    const token = $('meta[name="csrf-token"]').attr('content')
    const selectorForm = $("#form-beranda")
    const selectorButtonSubmitForm = $("#btnSubmitForm")
    const simpanForm = $("#simpanForm").val()

    selectorForm.on("submit", function (e) {
        e.preventDefault()
        let formObjectRequest = $(this).serializeObject()
        actionForm(formObjectRequest)
    })

    function actionForm(objectForm) {
        $.ajax({
            type: 'POST',
            url: simpanForm,
            headers: { 'X-CSRF-TOKEN': token },
            data: objectForm,
            beforeSend: () => {
                $('#btnSubmitForm').prop("disabled", true)
                $('#btnSubmitForm').html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Loading...')
            },
            success: function (response) {
                if(response.status == 'success'){
                    Swal.fire(
                        'Sukses!',
                        'Berhasil Mengirimkan Callback',
                        'success'
                    )
                    setTimeout(function(){
                        window.location = `/`;
                        $('#btnSubmitForm').prop("disabled", false)
                        $('#btnSubmitForm').html(`Simpan`)
                    }, 2000)
                }else{
                    Swal.fire(
                        'Gagal!',
                        response.messages,
                        'warning'
                    )
                    setTimeout(function(){
                        window.location = `/`;
                        $('#btnSubmitForm').prop("disabled", false)
                        $('#btnSubmitForm').html(`Simpan`)
                    }, 2000)
                }
            }
        })
    }
</script>
@endsection