@extends('layouts.master')

@section('title')
  <title>Dana Tunai - Jaminan Sertifikat Rumah</title>
@endsection
  
@section('content')
    <!-- Breadcrumb Section Begin -->
    <div class="breadcrumb-option contact-breadcrumb set-bg" data-setbg="{{ asset('tema/img/breadcrumb/breadcrumb-bg.jpg') }}">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="breadcrumb__text">
                        <h2>Ajukan dana tunai di sini</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Breadcrumb Section End -->
    <!-- Contact Begin -->
    <section class="contact spad">
        <div class="container">
            <div class="contact__form">
                <div class="row">
                    <div class="col-lg-1"></div>
                    <div class="col-lg-10">
                        <div class="contact__form__text hero__form">
                            <div class="contact__form__title">
                                <h2>Form Kelengkapan Data</h2>
                            </div>
                            <form class="call__form" id="form-dana-tunai">
                            <input type="hidden" id="simpanForm" value="{{ route('save-data-jaminan-rumah') }}">
                                <div class="input-list-width">
                                    <label><strong>Nama Lengkap</strong></label>
                                    <input type="text" name="nama_lengkap" placeholder="Tulis nama lengkap sesuai KTP di sini" required>
                                </div>
                                <div class="input-list-width">
                                    <label><strong>Nomor Telepon</strong></label>
                                    <input type="text" name="no_telp" placeholder="Tulis no telepon atau Whatsapp di sini" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" required>
                                </div>
                                <div class="input-list-width">
                                    <label><strong>Email</strong></label>
                                    <input type="email" name="email" placeholder="Tulis email di sini, kosongkan jika tidak ada">
                                </div>
                                <div class="input-list-width">
                                    <label><strong>Alamat</strong></label>
                                    <textarea name="alamat" placeholder="Tulis alamat lengkap sesuai KTP di sini" required></textarea>
                                </div>
                                <div class="input-list-width">
                                    <label><strong>Status Sertifikat Rumah</strong></label>
                                    <select name="status_bpkb" id="status_bpkb" required>
                                        <option value="">-- Pilih --</option>
                                        <option value="1">Kepemilikan Sendiri</option>
                                        <option value="2">Kepemilikan Orang Tua</option>
                                    </select>
                                </div>
                                <div class="input-list-width pemilikJaminan">
                                    <label><strong>Nama Pemilik Sertifikat</strong></label>
                                    <input type="text" name="pemilik_jaminan" placeholder="Tulis nama pemilik BPKB di sini">
                                </div>
                                <div class="input-list-width">
                                    <label><strong>Kode Referal</strong><span> (Opsional)</span></label>
                                    <input type="text" name="kode_referal" placeholder="Tulis kode referal di sini">
                                </div>
                                <button type="submit" class="site-btn" id="btnSubmitForm">Ajukan Sekarang</button>
                            </form>
                        </div>
                    </div>
                    <div class="col-lg-1"></div>
                </div>
            </div>
        </div>
    </section>
    <!-- Contact End -->

    <!-- Contact Begin -->
    <div class="contact-widget set-bg" data-setbg="{{ asset('tema/img/contact-widget.jpg') }}">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="contact__widget__item">
                                <h4>Alamat Kami</h4>
                                <p>{{ $footer->alamat }}</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="contact__widget__phone">
                        <span>Hubungi Kami Sekarang!</span>
                        <h2>{{ $footer->no_telp }}</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Contact End -->

@endsection
@section('scripts')
@parent
    <script type="text/javascript">
    
        const token = $('meta[name="csrf-token"]').attr('content')
        const selectorBpkpb = $("#status_bpkb")
        const fieldStatusBpkb = $(".pemilikJaminan")
        const selectorForm = $("#form-dana-tunai")
        const selectorButtonSubmitForm = $("#btnSubmitForm")
        const simpanForm = $("#simpanForm").val()

        let statusJaminan = $('select[name=status_bpkb] option').filter(':selected').val()

        selectorForm.on("submit", function (e) {
            e.preventDefault()
            let formObjectRequest = $(this).serializeObject()
            actionForm(formObjectRequest)
        })

        function initStatusJaminan() {
            selectorBpkpb.on("change", (e) => {
                e.preventDefault()
                value = e.target.value
                initHideField(value)
            })
        }

        function initHideField(statusJaminan) {
            if(statusJaminan == 1){
                fieldStatusBpkb.css("display", "none")
            }else if(statusJaminan == 2){
                fieldStatusBpkb.css("display", "block")
            }else{
                fieldStatusBpkb.css("display", "none")
            }
        }

        function actionForm(objectForm) {
            $.ajax({
                type: 'POST',
                url: simpanForm,
                headers: { 'X-CSRF-TOKEN': token },
                data: objectForm,
                success: function (response) {
                    if(response.status == 'success'){
                        Swal.fire(
                            'Sukses!',
                            'Berhasil Mengirimkan Data',
                            'success'
                        )
                        setTimeout(function(){
                            window.location = `/danatunai/jaminan-rumah`;
                            $('#btnSubmitForm').prop("disabled", false)
                            $('#btnSubmitForm').html(`Simpan`)
                        }, 2000)
                    }else{
                        Swal.fire(
                            'Gagal!',
                            response.messages,
                            'warning'
                        )
                        setTimeout(function(){
                            window.location = `/danatunai/jaminan-rumah`;
                            $('#btnSubmitForm').prop("disabled", false)
                            $('#btnSubmitForm').html(`Simpan`)
                        }, 2000)
                    }
                }
            })
        }

        function init() {
            initStatusJaminan()
            initHideField()
        }

        init()
        
    </script>
@endsection