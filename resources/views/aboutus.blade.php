@extends('layouts.master')

@section('title')
  <title>Tentang Kami - Cepatcair.com</title>
@endsection
  
@section('content')
    <!-- Breadcrumb Section Begin -->
    <div class="breadcrumb-option set-bg" data-setbg="{{ asset('tema/img/breadcrumb/breadcrumb-bg.jpg') }}">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="breadcrumb__text">
                        <h2>Tentang Kami</h2>
                        <div class="breadcrumb__links">
                            <a href="{{ route('home') }}">Beranda</a>
                            <span>About</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Breadcrumb Section End -->
     <!-- About Section Begin -->
     <section class="about spad">
        <div class="container">
            <div class="about__content">
                <div class="row">
                    <div class="col-lg-5">
                        <div class="about__img">
                            <img src="{{ asset('tema/img/about/about.jpg') }}" alt="">
                            <a href="{{ $aboutus->url_video }}"
                                class="play-btn video-popup"><img src="{{ asset('tema/img/about/video-play.png') }}" alt=""></a>
                        </div>
                    </div>
                    <div class="col-lg-6 offset-lg-1">
                        <div class="about__text">
                            <h2>{{ $aboutus->judul }}</h2>
                            <h4>{{ $aboutus->pengantar }}</h4>
                            <p class="last_para">{{ $aboutus->isi }}</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-4">
                    <div class="about__item">
                        <h4>{{ $aboutus->judul_visi }}</h4>
                        <p>{{ $aboutus->isi_visi }}</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4">
                    <div class="about__item">
                        <h4>{{ $aboutus->judul_misi }}</h4>
                        <p>{{ $aboutus->isi_misi }}</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4">
                    <div class="about__item">
                        <h4>{{ $aboutus->judul_value }}</h4>
                        <p>{{ $aboutus->isi_value }}</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- About End -->

    <!-- Choose Section Begin -->
    <section class="choose spad set-bg" data-setbg="{{ asset('tema/img/history/history-bg.jpg') }}">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title">
                        <h2><font color="#fff">{{ $landingPage->teks_3 }}</font></h2>
                        <p><font color="#fff">{{ $landingPage->deskripsi_3 }}</font></p>
                    </div>
                </div>
            </div>
            <div class="row">
                @foreach($keunggulan as $keunggulan)
                    <div class="col-lg-4 col-md-4 col-sm-6">
                        <div class="choose__item">
                            <img src="{{ asset('uploads/keunggulan/'.$keunggulan->icon) }}" alt="">
                            <h5>{{ $keunggulan->title }}</h5>
                            <p>{{ $keunggulan->description }}</p>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
    <!-- Choose Section End -->

    <!-- Counter Begin -->
    <div class="counter spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-6">
                    <div class="counter__item">
                        <img src="{{ asset('tema/img/counter/counter-1.png') }}" alt="">
                        <div class="counter__number">
                            <h2 class="counter-add">2100</h2>
                        </div>
                        <p>Successful Loan Approval</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6">
                    <div class="counter__item">
                        <img src="{{ asset('tema/img/counter/counter-2.png') }}" alt="">
                        <div class="counter__number">
                            <h2 class="counter-add">99</h2>
                            <span>%</span>
                        </div>
                        <p>Customer Satisfection</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6">
                    <div class="counter__item">
                        <img src="{{ asset('tema/img/counter/counter-3.png') }}" alt="">
                        <div class="counter__number">
                            <h2 class="counter-add">90</h2>
                            <span>+</span>
                        </div>
                        <p>Office National Partners</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6">
                    <div class="counter__item">
                        <img src="{{ asset('tema/img/counter/counter-4.png') }}" alt="">
                        <div class="counter__number">
                            <h2 class="counter-add">70</h2>
                            <span>+</span>
                        </div>
                        <p>Award Certificate</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Counter End -->

    <!-- Contact Begin -->
    <div class="contact-widget set-bg" data-setbg="{{ asset('tema/img/contact-widget.jpg') }}">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="contact__widget__item">
                                <h4>Alamat Kami</h4>
                                <p>{{ $footer->alamat }}</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="contact__widget__phone">
                        <span>Hubungi Kami Sekarang!</span>
                        <h2>{{ $footer->no_telp }}</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Contact End -->
@endsection