@extends('layouts.master')

@section('title')
  <title>Kontak Kami - Cepatcair.com</title>
@endsection
  
@section('content')
    <!-- Breadcrumb Section Begin -->
    <div class="breadcrumb-option contact-breadcrumb set-bg" data-setbg="{{ asset('tema/img/breadcrumb/breadcrumb-bg.jpg') }}">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="breadcrumb__text">
                        <h2>Kontak Kami</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Breadcrumb Section End -->
    <!-- Contact Begin -->
    <section class="contact spad">
        <div class="container">
            <div class="contact__form">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="contact__form__text">
                            <div class="contact__form__title">
                                <h2>Hubungi Kami</h2>
                            </div>
                            <form id="form-contact-us">
                                <input type="hidden" id="simpanForm" value="{{ route('send-message') }}">
                                <div class="input-list">
                                    <input type="text" name="name" placeholder="Nama" required>
                                    <input type="email" name="email" placeholder="Email" required>
                                </div>
                                <textarea name="message" placeholder="Tulis pesan di sini..." required></textarea>
                                <button type="submit" class="site-btn" id="btnSubmitForm">Kirim Pesan</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Contact End -->

    <!-- Contact Begin -->
    <div class="contact-widget set-bg" data-setbg="{{ asset('tema/img/contact-widget.jpg') }}">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="contact__widget__item">
                                <h4>Alamat Kami</h4>
                                <p>{{ $footer->alamat }}</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="contact__widget__phone">
                        <span>Hubungi Kami Sekarang!</span>
                        <h2>{{ $footer->no_telp }}</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Contact End -->

@endsection
@section('scripts')
@parent
<script type="text/javascript">
    
    const token = $('meta[name="csrf-token"]').attr('content')
    const selectorForm = $("#form-contact-us")
    const selectorButtonSubmitForm = $("#btnSubmitForm")
    const simpanForm = $("#simpanForm").val()

    selectorForm.on("submit", function (e) {
        e.preventDefault()
        let formObjectRequest = $(this).serializeObject()
        actionForm(formObjectRequest)
    })

    function actionForm(objectForm) {
        $.ajax({
            type: 'POST',
            url: simpanForm,
            headers: { 'X-CSRF-TOKEN': token },
            data: objectForm,
            beforeSend: () => {
                $('#btnSubmitForm').prop("disabled", true)
                $('#btnSubmitForm').html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Loading...')
            },
            success: function (response) {
                if(response.status == 'success'){
                    Swal.fire(
                        'Sukses!',
                        'Berhasil Mengirimkan Pesan',
                        'success'
                    )
                    setTimeout(function(){
                        window.location = `/contactus`;
                        $('#btnSubmitForm').prop("disabled", false)
                        $('#btnSubmitForm').html(`Simpan`)
                    }, 2000)
                }else{
                    Swal.fire(
                        'Gagal!',
                        response.messages,
                        'warning'
                    )
                    setTimeout(function(){
                        window.location = `/contactus`;
                        $('#btnSubmitForm').prop("disabled", false)
                        $('#btnSubmitForm').html(`Simpan`)
                    }, 2000)
                }
            }
        })
    }
</script>
@endsection