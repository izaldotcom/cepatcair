<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

//Send Callback
Route::post('/send-callback', 'HomeController@sendCallback')->name('send-callback');

// About Us
Route::resource('/aboutus', 'AboutusController');

// Services
Route::resource('/services', 'ServicesController');

// Dana Tunai
Route::get('/danatunai/jaminan-rumah', 'DanaTunaiController@indexJaminanRumah')->name('jaminan-rumah');
Route::post('/save-data-jaminan-rumah', 'DanaTunaiController@saveDataJaminanRumah')->name('save-data-jaminan-rumah');

Route::get('/danatunai/jaminan-motor', 'DanaTunaiController@indexJaminanMotor')->name('jaminan-motor');
Route::post('/save-data-jaminan-motor', 'DanaTunaiController@saveDataJaminanMotor')->name('save-data-jaminan-motor');

Route::get('/danatunai/jaminan-mobil', 'DanaTunaiController@indexJaminanMobil')->name('jaminan-mobil');
Route::post('/save-data-jaminan-mobil', 'DanaTunaiController@saveDataJaminanMobil')->name('save-data-jaminan-mobil');
// Route::resource('/danatunai', 'DanaTunaiController');

// ContactUs
Route::post('/send', 'ContactUsController@send')->name('send-message');
Route::resource('/contactus', 'ContactUsController');

Route::group(['prefix' => 'backend', 'middleware'=>['auth']], function () {
    Route::get('/home', 'Backend\HomeController@index')->name('backend.home');
    
    Route::group(['prefix' => 'homepage', 'middleware'=>['auth', 'role:superadmin']], function () {

        //Menu
        Route::get('/menufront/edit/{id}', 'Backend\Homepage\MenuFrontController@edit')->name('menufront.edit');
        Route::get('/menufront/delete/{id}', 'Backend\Homepage\MenuFrontController@destroy')->name('menufront.delete');
        Route::resource('/menufront', 'Backend\Homepage\MenuFrontController');

        //SubMenu
        Route::get('/submenufront/edit/{id}', 'Backend\Homepage\SubMenuFrontController@edit')->name('submenufront.edit');
        Route::get('/submenufront/delete/{id}', 'Backend\Homepage\SubMenuFrontController@destroy')->name('submenufront.delete');
        Route::resource('/submenufront', 'Backend\Homepage\SubMenuFrontController');

        //LoanType
        Route::get('/loantype/edit/{id}', 'Backend\Homepage\LoanTypeController@edit')->name('loantype.edit');
        Route::get('/loantype/delete/{id}', 'Backend\Homepage\LoanTypeController@destroy')->name('loantype.delete');
        Route::resource('/loantype', 'Backend\Homepage\LoanTypeController');

        // Footer
        Route::resource('/footer', 'Backend\Homepage\FooterController');

        // AboutUs
        Route::resource('/aboutus', 'Backend\Homepage\AboutusController');

        // Landing Page
        Route::resource('/landingpage', 'Backend\Homepage\LandingPageController');

        //Keunggulan
        Route::get('/keunggulan/edit/{id}', 'Backend\Homepage\KeunggulanController@edit')->name('keunggulan.edit');
        Route::get('/keunggulan/delete/{id}', 'Backend\Homepage\KeunggulanController@destroy')->name('keunggulan.delete');
        Route::resource('/keunggulan', 'Backend\Homepage\KeunggulanController');

        //Callback
        Route::get('/callback/edit/{id}', 'Backend\Homepage\CallbackController@edit')->name('callback.edit');
        Route::get('/callback/delete/{id}', 'Backend\Homepage\CallbackController@destroy')->name('callback.delete');
        Route::resource('/callback', 'Backend\Homepage\CallbackController');

        // Kontak Kami / Pesan
        Route::get('/contactus/edit/{id}', 'Backend\Homepage\ContactUsController@edit')->name('contactus.edit');
        Route::get('/contactus/delete/{id}', 'Backend\Homepage\ContactUsController@destroy')->name('contactus.delete');
        Route::resource('/contactus', 'Backend\Homepage\ContactUsController');
    });
    
    Route::group(['prefix' => 'transaction', 'middleware'=>['auth', 'role:superadmin']], function () {

        // Dana Tunai
        Route::get('/danatunai/edit/{id}', 'Backend\Transaction\DanaTunaiController@edit')->name('danatunai.edit');
        Route::get('/danatunai/delete/{id}', 'Backend\Transaction\DanaTunaiController@destroy')->name('danatunai.delete');
        Route::resource('/danatunai', 'Backend\Transaction\DanaTunaiController');

    });

    Route::group(['prefix' => 'utilitas', 'middleware'=>['auth', 'role:superadmin']], function () {
        Route::get('/user/edit/{id}', 'Backend\Utilitas\UserController@edit')->name('user.edit');
        Route::get('/user/roles/{id}', 'Backend\Utilitas\UserController@roles')->name('user.roles');
        Route::put('/user/roles/{id}', 'Backend\Utilitas\UserController@setRole')->name('user.set_role');
        Route::post('/user/permission', 'Backend\Utilitas\UserController@addPermission')->name('user.add_permission');
        Route::get('/user/role-permission', 'Backend\Utilitas\UserController@rolePermission')->name('user.roles_permission');
        Route::put('/user/permission/{role}', 'Backend\Utilitas\UserController@setRolePermission')->name('user.setRolePermission');
    	Route::resource('/user', 'Backend\Utilitas\UserController');

        Route::resource('/role', 'Backend\Utilitas\RoleController');

        //Menu
        Route::get('/menu/edit/{id}', 'Backend\Utilitas\MenuController@edit')->name('menu.edit');
        Route::get('/menu/delete/{id}', 'Backend\Utilitas\MenuController@destroy')->name('menu.delete');
    	Route::resource('/menu', 'Backend\Utilitas\MenuController');

        //SubMenu
        Route::get('/submenu/edit/{id}', 'Backend\Utilitas\SubMenuController@edit')->name('submenu.edit');
        Route::get('/submenu/delete/{id}', 'Backend\Utilitas\SubMenuControllerr@destroy')->name('submenu.delete');
    	Route::resource('/submenu', 'Backend\Utilitas\SubMenuController');

        ///Admin
        Route::get('/admin/edit/{id}', 'Backend\Utilitas\AdminController@edit')->name('admin.edit');
        Route::get('/admin/delete/{id}', 'Backend\Utilitas\AdminController@destroy')->name('admin.delete');
        Route::resource('/admin', 'Backend\Utilitas\AdminController');

        //Customer
        Route::resource('/customer', 'Backend\Utilitas\CustomerController');

        //Notification
        Route::resource('/notification', 'Backend\Utilitas\NotificationController');
    });
});


Route::group(['prefix' => 'master', 'middleware'=>['auth']], function () {

    //Banner
    Route::get('/banner/edit/{id}', 'Master\BannerController@edit')->name('banner.edit');
    Route::get('/banner/delete/{id}', 'Master\BannerController@destroy')->name('banner.delete');
	Route::resource('/banner', 'Master\BannerController');

    // Brand
    Route::get('/brand/edit/{id}', 'Master\BrandController@edit')->name('brand.edit');
    Route::get('/brand/delete/{id}', 'Master\BrandController@destroy')->name('brand.delete');
    Route::get('/brand/product-to-brand/{id}', 'Master\BrandController@productToBrand')->name('brand.product-to-brand');
    Route::get('/brand/get-product', 'Master\BrandController@getProduct')->name('brand.get-product');
    Route::post('/brand/save-brand-to-product', 'Master\BrandController@saveProductToBrand')->name('brand.save-brand-to-product');
    Route::resource('/brand', 'Master\BrandController');

    // Category
    Route::get('/productcategory/edit/{id}', 'Master\ProductCategoryController@edit')->name('productcategory.edit');
    Route::resource('/productcategory', 'Master\ProductCategoryController');

    // Product
    Route::get('/product/edit/{id}', 'Master\ProductController@edit')->name('product.edit');
    Route::resource('/product', 'Master\ProductController');
});

Route::group(['prefix' => 'transaction', 'middleware'=>['auth']], function () {

    //Inquiry
    Route::get('/inquiry/edit/{id}', 'Transaction\InquiryController@edit')->name('inquiry.edit');
    Route::resource('/inquiry', 'Transaction\InquiryController');

    //Quotation
    Route::get('/quotation/detail/{id}', 'Transaction\QuotationController@detail')->name('quotation.detail');
    Route::resource('/quotation', 'Transaction\QuotationController');

    //Order
    Route::get('/order/detail/{id}', 'Transaction\OrderController@detail')->name('order.detail');
    Route::post('/order/save-receipt-number/{id}', 'Transaction\OrderController@saveReceiptNumber')->name('order.save-receipt-number');
    Route::resource('/order', 'Transaction\OrderController');
});

Route::group(['prefix' => 'log', 'middleware'=>['auth']], function () {

    //General Log
    Route::resource('/general-log', 'Log\GeneralLogController');

    //Midtrans Log
    Route::resource('/midtrans-log', 'Log\MidtransLogController');

    //Thirdparty Log
    Route::resource('/thirdparty-log', 'Log\ThirdpartyLogController');
});

Route::group(['prefix' => 'report', 'middleware'=>['auth']], function () {

    //Inquiry Report
    Route::get('/print-inquiry/{status?}/{start_date?}/{end_date?}', 'Report\InquiryReportController@print');
    Route::get('/excel-inquiry/{status?}/{start_date?}/{end_date?}', 'Report\InquiryReportController@excel');
    Route::resource('/inquiry-report', 'Report\InquiryReportController');

    //Quotation Report
    Route::get('/print-quotation/{status?}/{start_date?}/{end_date?}', 'Report\QuotationReportController@print');
    Route::get('/excel-quotation/{status?}/{start_date?}/{end_date?}', 'Report\QuotationReportController@excel');
    Route::resource('/quotation-report', 'Report\QuotationReportController');

    //Order Report
    Route::get('/print-order/{status?}/{start_date?}/{end_date?}', 'Report\OrderReportController@print');
    Route::get('/excel-order/{status?}/{start_date?}/{end_date?}', 'Report\OrderReportController@excel');
    Route::resource('/order-report', 'Report\OrderReportController');
});