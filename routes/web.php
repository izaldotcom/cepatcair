<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

// Illuminate\Auth\Middleware\Authenticate
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/',function(){
    return redirect(route('home'));
});

Route::get('/', [
    'as' => 'home',
    'uses' => 'HomeController@index'
]);

Route::get('/login-system', '\App\Http\Controllers\Auth\LoginController@index')->name('login-system');
Route::post('/process-login', '\App\Http\Controllers\Auth\LoginController@processLogin')->name('process-login');
Route::post('logout-system', '\App\Http\Controllers\Auth\LoginController@logout')->name('logout-system');

Auth::routes();