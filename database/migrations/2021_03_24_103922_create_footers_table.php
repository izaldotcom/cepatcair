<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFootersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('footers', function (Blueprint $table) {
            $table->id();
            $table->longtext('alamat')->nullable();
            $table->string('no_telp', 255)->nullable();
            $table->string('email', 255)->nullable();
            $table->longtext('jam_operasional')->nullable();
            $table->string('url_fb', 255)->nullable();
            $table->string('url_twitter', 255)->nullable();
            $table->string('url_ig', 255)->nullable();
            $table->longtext('deskripsi')->nullable();
            $table->timestamps();
            $table->softDeletes('deleted_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('footers');
    }
}
