<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubmenuFrontsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('submenu_fronts', function (Blueprint $table) {
            $table->id();
            $table->foreignId('menu_id');
            $table->string('name', 100)->nullable();
            $table->string('url', 255)->nullable();
            $table->tinyInteger('status');
            $table->tinyInteger('no_order');
            $table->timestamps();
            $table->softDeletes('deleted_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('submenu_fronts');
    }
}
