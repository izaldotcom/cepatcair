<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrDanaTunaiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tr_dana_tunai', function (Blueprint $table) {
            $table->id();
            $table->string('nama_lengkap', 255);
            $table->string('no_telp', 255);
            $table->string('email', 255);
            $table->longtext('alamat');
            $table->tinyInteger('status_bpkb');
            $table->tinyInteger('tipe');
            $table->string('pemilik_jaminan', 255)->nullable();
            $table->string('kode_referal', 255)->nullable();
            $table->timestamps();
            $table->softDeletes('deleted_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tr_dana_tunai');
    }
}
