<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAboutusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aboutus', function (Blueprint $table) {
            $table->id();
            $table->longtext('judul')->nullable();
            $table->longtext('pengantar')->nullable();
            $table->longtext('isi')->nullable();
            $table->string('judul_visi', 255)->nullable();
            $table->longtext('isi_visi')->nullable();
            $table->string('judul_misi', 255)->nullable();
            $table->longtext('isi_misi')->nullable();
            $table->string('judul_value', 255)->nullable();
            $table->longtext('isi_value')->nullable();
            $table->longtext('url_video')->nullable();
            $table->timestamps();
            $table->softDeletes('deleted_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aboutus');
    }
}
