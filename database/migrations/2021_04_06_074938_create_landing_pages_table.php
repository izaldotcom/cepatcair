<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLandingPagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('landing_pages', function (Blueprint $table) {
            $table->id();
            $table->string('banner');
            $table->longtext('teks_1')->nullable();
            $table->longtext('deskripsi_1')->nullable();
            $table->longtext('teks_2')->nullable();
            $table->longtext('deskripsi_2')->nullable();
            $table->longtext('teks_3')->nullable();
            $table->longtext('deskripsi_3')->nullable();
            $table->timestamps();
            $table->softDeletes('deleted_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('landing_pages');
    }
}
