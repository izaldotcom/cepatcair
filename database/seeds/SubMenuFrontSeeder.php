<?php

use Illuminate\Database\Seeder;
use App\Models\SubMenuFront;

class SubMenuFrontSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SubMenuFront::create([
            'menu_id' => 4,
            'name' => 'Sertifikat Rumah',
            'url' => '/danatunai/jaminan-rumah',
            'status' => 1,
            'no_order' => 1,
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        SubMenuFront::create([
            'menu_id' => 4,
            'name' => 'BPKB Motor',
            'url' => '/danatunai/jaminan-motor',
            'status' => 1,
            'no_order' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        SubMenuFront::create([
            'menu_id' => 4,
            'name' => 'BPKB Mobil',
            'url' => '/danatunai/jaminan-mobil',
            'status' => 1,
            'no_order' => 3,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }
}
