<?php

use Illuminate\Database\Seeder;
use App\Models\Menu;
class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Menu::create([
            'name' => 'Homepage',
            'icon' => 'fa-book',
            'url' => '#',
            'description' => 'Menu Homepage',
            'is_parent' => 1,
            'no_order' => 1,
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        Menu::create([
            'name' => 'Transaction',
            'icon' => 'fa-book',
            'url' => '#',
            'description' => 'Menu Transaksi',
            'is_parent' => 1,
            'no_order' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }
}
