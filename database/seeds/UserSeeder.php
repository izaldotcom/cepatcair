<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = User::create([
            'name' => 'administrator',
            'email' => 'admin@cepatcair.com',
            'email_verified_at' => now(),
            'password' => bcrypt('12345678'),
            'status' => 'aktif',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        $admin->assignRole('superadmin');
    }
}
