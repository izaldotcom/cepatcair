<?php

use Illuminate\Database\Seeder;
use App\Models\SubMenu;
class SubMenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SubMenu::create([
            'menu_id' => 1,
            'name' => 'Menu Front',
            'icon' => 'fa-circle',
            'url' => '/backend/homepage/menufront',
            'description' => null,
            'status' => 1,
            'no_order' => 1,
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        SubMenu::create([
            'menu_id' => 2,
            'name' => 'Dana Tunai',
            'icon' => 'fa-circle',
            'url' => '/backend/transaction/danatunai',
            'description' => null,
            'status' => 1,
            'no_order' => 1,
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        SubMenu::create([
            'menu_id' => 1,
            'name' => 'Submenu Front',
            'icon' => 'fa-circle',
            'url' => '/backend/homepage/submenufront',
            'description' => null,
            'status' => 1,
            'no_order' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        SubMenu::create([
            'menu_id' => 1,
            'name' => 'Landing Page',
            'icon' => 'fa-circle',
            'url' => '/backend/homepage/landingpage',
            'description' => null,
            'status' => 1,
            'no_order' => 3,
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        SubMenu::create([
            'menu_id' => 1,
            'name' => 'Loan Type',
            'icon' => 'fa-circle',
            'url' => '/backend/homepage/loantype',
            'description' => null,
            'status' => 1,
            'no_order' => 4,
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        SubMenu::create([
            'menu_id' => 1,
            'name' => 'Keunggulan',
            'icon' => 'fa-circle',
            'url' => '/backend/homepage/keunggulan',
            'description' => null,
            'status' => 1,
            'no_order' => 5,
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        SubMenu::create([
            'menu_id' => 1,
            'name' => 'Callback',
            'icon' => 'fa-circle',
            'url' => '/backend/homepage/callback',
            'description' => null,
            'status' => 1,
            'no_order' => 6,
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        SubMenu::create([
            'menu_id' => 1,
            'name' => 'Footer',
            'icon' => 'fa-circle',
            'url' => '/backend/homepage/footer',
            'description' => null,
            'status' => 1,
            'no_order' => 7,
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        SubMenu::create([
            'menu_id' => 1,
            'name' => 'About us',
            'icon' => 'fa-circle',
            'url' => '/backend/homepage/aboutus',
            'description' => null,
            'status' => 1,
            'no_order' => 8,
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        SubMenu::create([
            'menu_id' => 1,
            'name' => 'Kontak Kami',
            'icon' => 'fa-circle',
            'url' => '/backend/homepage/contactus',
            'description' => null,
            'status' => 1,
            'no_order' => 9,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }
}
