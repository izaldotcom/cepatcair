<?php

use Illuminate\Database\Seeder;
use App\Models\MenuFront;

class MenuFrontSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MenuFront::create([
            'name' => 'Tentang Kami',
            'url' => '/aboutus',
            'is_parent' => 1,
            'no_order' => 1,
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        MenuFront::create([
            'name' => 'Layanan',
            'url' => '/services',
            'is_parent' => 1,
            'no_order' => 2,
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        MenuFront::create([
            'name' => 'Dana Tunai',
            'url' => '/product',
            'is_parent' => 1,
            'no_order' => 3,
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        MenuFront::create([
            'name' => 'Kontak Kami',
            'url' => '/contactus',
            'is_parent' => 1,
            'no_order' => 4,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }
}
