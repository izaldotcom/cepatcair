<?php

namespace App\Http\Controllers\Backend\Transaction;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use App\Models\TrDanaTunai;

class DanaTunaiController extends Controller
{

    public function index()
	{
		if(request()->ajax())
        {

    		$data = TrDanaTunai::whereNull('deleted_at')->orderBy('id', 'ASC')->get();

            return datatables()->of($data)
				->addColumn('tipe', static function ($row) {
					$valTipe = '';
					if($row->tipe == TrDanaTunai::RUMAH){
						$valTipe = 'Rumah';
					}elseif($row->tipe == TrDanaTunai::MOTOR){
						$valTipe = 'Motor';
					}else{
						$valTipe = 'Mobil';
					}

					return $valTipe;
				})
                ->addColumn('idTrDanaTunai', static function ($row) {
                    return \Crypt::encrypt($row->id);
                })
                ->rawColumns(['tipe', 'idTrDanaTunai'])
                ->make(true);
        }

    	return view('backend.transaction.danatunai.index');
	}

    public function destroy($id)
    { 
    	$id = (int)\Crypt::decrypt($id);
	    $data = TrDanaTunai::findOrFail($id);
	    $data->delete();

	    self::danger('Transaction has been deleted.');
	    return redirect()->back();
    }

    public function edit($id)
	{
		$id = (int)\Crypt::decrypt($id);
	    $data = TrDanaTunai::findOrFail($id);
	    return view('backend.transaction.danatunai.edit', compact('data'));
	}

	public function update(Request $request, $id)
    {  
	    $this->validate($request, [
	        'icon' => 'image|mimes:jpg,png,jpeg',
	        'title' => 'string|max:100',
	        'description' => 'string|nullable',
	        'updated_at' => date("Y-m-d H:i:s")
	    ]);

	    try {
	        $data = TrDanaTunai::findOrFail($id);
            $foto = $data->icon;

	        if ($request->hasFile('icon')) {
	            !empty($foto) ? File::delete(public_path('uploads/keunggulan/' . $foto)):null;
	            $foto = $this->saveImage($request->title, $request->file('icon'));
	        }

	        $data->update([
	        	'icon' => $foto,
	            'title' => $request->title,
	            'description' => $request->description,
	            'updated_at' => date("Y-m-d H:i:s")
	        ]);
	        
		    self::success('Data has been updated.');
			return redirect(route('callback.index'));
	    } catch (\Exception $e) {
	    	self::danger('Data failed to update.');
	        return redirect()->back();
	    }
    }
}
