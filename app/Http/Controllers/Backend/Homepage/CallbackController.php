<?php

namespace App\Http\Controllers\Backend\Homepage;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use App\Models\Callback;

class CallbackController extends Controller
{

    public function index()
	{
		if(request()->ajax())
        {

    		$data = Callback::whereNull('deleted_at')->orderBy('id', 'ASC')->get();

            return datatables()->of($data)
                ->addColumn('idCallback', static function ($row) {
                    return \Crypt::encrypt($row->id);
                })
                ->rawColumns(['idCallback'])
                ->make(true);
        }

    	return view('backend.homepage.callback.index');
	}

    public function destroy($id)
    { 
    	$id = (int)\Crypt::decrypt($id);
	    $data = Callback::findOrFail($id);
	    $data->delete();

	    self::danger('Data has been deleted.');
	    return redirect()->back();
    }

    public function edit($id)
	{
		$id = (int)\Crypt::decrypt($id);
	    $data = Callback::findOrFail($id);
	    return view('backend.homepage.callback.edit', compact('data'));
	}

	public function update(Request $request, $id)
    {  
	    $this->validate($request, [
	        'icon' => 'image|mimes:jpg,png,jpeg',
	        'title' => 'string|max:100',
	        'description' => 'string|nullable',
	        'updated_at' => date("Y-m-d H:i:s")
	    ]);

	    try {
	        $data = Callback::findOrFail($id);
            $foto = $data->icon;

	        if ($request->hasFile('icon')) {
	            !empty($foto) ? File::delete(public_path('uploads/keunggulan/' . $foto)):null;
	            $foto = $this->saveImage($request->title, $request->file('icon'));
	        }

	        $data->update([
	        	'icon' => $foto,
	            'title' => $request->title,
	            'description' => $request->description,
	            'updated_at' => date("Y-m-d H:i:s")
	        ]);
	        
		    self::success('Data has been updated.');
			return redirect(route('callback.index'));
	    } catch (\Exception $e) {
	    	self::danger('Data failed to update.');
	        return redirect()->back();
	    }
    }
}
