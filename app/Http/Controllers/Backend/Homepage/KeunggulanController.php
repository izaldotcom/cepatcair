<?php

namespace App\Http\Controllers\Backend\Homepage;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use App\Models\Keunggulan;
use App\Models\SubKeunggulan;
use File;
use Intervention\Image\ImageManagerStatic as Image;

class KeunggulanController extends Controller
{

    public function index()
	{
		if(request()->ajax())
        {

    		$data = Keunggulan::whereNull('deleted_at')->orderBy('id', 'ASC')->get();

            return datatables()->of($data)
                ->addColumn('ikon', static function ($row) {
                    $url= asset('uploads/keunggulan/'.$row->icon);

                    if(!empty($row->icon)){
                        return '<img src="'.$url.'" alt="'.$row->title.'" width="100%" style="max-width: 150px;"/>';
                    }else{
                        return '<img src="http://via.placeholder.com/50x50" alt="'.$row->title.'">';
                    }
                })
                ->addColumn('idKeunggulan', static function ($row) {
                    return \Crypt::encrypt($row->id);
                })
                ->rawColumns(['ikon', 'idKeunggulan'])
                ->make(true);
        }

    	return view('backend.homepage.keunggulan.index');
	}

	public function create()
	{
	    return view('backend.homepage.keunggulan.create');
	}

    public function saveImage($name, $foto)
    {     

	    $images = Str::slug($name) . time() . '.' . $foto->getClientOriginalExtension();
	    $path = public_path('uploads/keunggulan');

	    if (!File::isDirectory($path)) {
	        File::makeDirectory($path, 0777, true, true);
	    }	

	    Image::make($foto)->save($path . '/' . $images);
	    return $images;
    }

	public function store(Request $request)
    {	        
        //validasi data
	    $this->validate($request, [
            'icon' => 'image|mimes:jpg,png,jpeg',
	        'title' => 'string|max:100',
	        'description' => 'string|nullable',
	        'created_at' => date("Y-m-d H:i:s")
	    ]);	

	    try {

            $foto = null;
	        if ($request->hasFile('icon')) {
	            $foto = $this->saveImage($request->title, $request->file('icon'));
	        }
	        
	        $data = Keunggulan::create([
	            'icon' => $foto,
	            'title' => $request->title,
	            'description' => $request->description,
	        ]);

	        self::success('Data has been added.');
	        return redirect(route('keunggulan.index'));
	    } catch (\Exception $e) {
	        self::danger('Adding Data has been failed.');
	        return redirect()->back();
	    }
    }

    public function destroy($id)
    { 
    	$id = (int)\Crypt::decrypt($id);
	    $data = Keunggulan::findOrFail($id);
	    $data->delete();

	    self::danger('Data has been deleted.');
	    return redirect()->back();
    }

    public function edit($id)
	{
		$id = (int)\Crypt::decrypt($id);
	    $data = Keunggulan::findOrFail($id);
	    return view('backend.homepage.keunggulan.edit', compact('data'));
	}

	public function update(Request $request, $id)
    {  
	    $this->validate($request, [
	        'icon' => 'image|mimes:jpg,png,jpeg',
	        'title' => 'string|max:100',
	        'description' => 'string|nullable',
	        'updated_at' => date("Y-m-d H:i:s")
	    ]);

	    try {
	        $data = Keunggulan::findOrFail($id);
            $foto = $data->icon;

	        if ($request->hasFile('icon')) {
	            !empty($foto) ? File::delete(public_path('uploads/keunggulan/' . $foto)):null;
	            $foto = $this->saveImage($request->title, $request->file('icon'));
	        }

	        $data->update([
	        	'icon' => $foto,
	            'title' => $request->title,
	            'description' => $request->description,
	            'updated_at' => date("Y-m-d H:i:s")
	        ]);
	        
		    self::success('Data has been updated.');
			return redirect(route('keunggulan.index'));
	    } catch (\Exception $e) {
	    	self::danger('Data failed to update.');
	        return redirect()->back();
	    }
    }
}
