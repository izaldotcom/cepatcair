<?php

namespace App\Http\Controllers\Backend\Homepage;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\MenuFront;
use App\Models\SubMenuFront;

class SubMenuFrontController extends Controller
{

    public function index()
	{
		if(request()->ajax())
        {

    		$data = SubMenuFront::whereNull('deleted_at')->orderBy('no_order', 'ASC')->get();

            return datatables()->of($data)
            	->addColumn('menu', static function ($row) {
            		$menu = MenuFront::where('id', $row->menu_id)->first();
                	return $menu->name;
                })
                ->addColumn('status', static function ($row) {
                	if($row->status == 0){
                		return '<span class="badge bg-danger">Inactive</span>';
                	}else{
                		return '<span class="badge bg-success">Active</span>';
                	}
                })
                ->addColumn('idSubMenu', static function ($row) {
                    return \Crypt::encrypt($row->id);
                })
                ->rawColumns(['menu', 'status', 'idSubMenu'])
                ->make(true);
        }

    	return view('backend.homepage.submenu.index');
	}

	public function create()
	{
		$menu = MenuFront::orderBy('name', 'ASC')->get();
	    return view('backend.homepage.submenu.create', compact('menu'));
	}

	public function store(Request $request)
    {	        
        //validasi data
	    $this->validate($request, [
	    	'menu_id' => 'required|integer',
	        'name' => 'string|max:100',
	        'url' => 'nullable|string|max:100',
	        'status' => 'integer',
	        'no_order' => 'integer',
	        'created_at' => date("Y-m-d H:i:s")
	    ]);	

	    try {
	        
	        $submenu = SubMenuFront::create([
	            'menu_id' => $request->menu_id,
	            'name' => $request->name,
	            'url' => $request->url,
	            'status' => $request->status,
	            'no_order' => $request->no_order
	        ]);
	        
	        self::success('Submenu has been added.');
	        return redirect(route('submenufront.index'));
	    } catch (\Exception $e) {
	        self::danger('Adding submenu has been failed.');
	        return redirect()->back();
	    }
    }

    public function destroy($id)
    { 
    	$id = (int)\Crypt::decrypt($id);
	    $submenu = SubMenuFront::findOrFail($id);
	    // $submenu = SubMenuFront::where('id_menu', $submenu->id)->delete();
	    //$permission = Permission::where('menu_name', str_replace(' ', '', strtolower($submenu->name)))->delete();
	    $submenu->delete();

	    self::danger('Submenu has been deleted.');
	    return redirect()->back();
    }

    public function edit($id)
	{
		$id = (int)\Crypt::decrypt($id);
		$menu = MenuFront::orderBy('name', 'ASC')->get();
	    $submenu = SubMenuFront::findOrFail($id);
	    return view('backend.homepage.submenu.edit', compact('submenu', 'menu'));
	}

	public function update(Request $request, $id)
    {  
	    $this->validate($request, [
	    	'menu_id' => 'required|integer',
	        'name' => 'string|max:100',
	        'url' => 'nullable|string|max:100',
	        'status' => 'integer',
	        'no_order' => 'integer',
	        'updated_at' => date("Y-m-d H:i:s")
	    ]);

	    try {
	        $submenu = SubMenuFront::findOrFail($id);

	        $submenu->update([
	        	'menu_id' => $request->menu_id,
	        	'name' => $request->name,
	            'url' => $request->url,
	            'status' => $request->status,
	            'no_order' => $request->no_order,
	            'updated_at' => date("Y-m-d H:i:s")
	        ]);
	        
		    self::success('Submenu has been updated.');
			return redirect(route('submenufront.index'));
	    } catch (\Exception $e) {
	    	self::danger('Data submenu failed to update.');
	        return redirect()->back();
	    }
    }
}
