<?php

namespace App\Http\Controllers\Backend\Homepage;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\MenuFront;
use App\Models\SubMenuFront;

class MenuFrontController extends Controller
{

    public function index()
	{
		if(request()->ajax())
        {

    		$data = MenuFront::whereNull('deleted_at')->orderBy('no_order', 'ASC')->get();

            return datatables()->of($data)
                ->addColumn('is_parent', static function ($row) {
                	if($row->is_parent == 0){
                		return '<span class="badge bg-danger">No</span>';
                	}else{
                		return '<span class="badge bg-success">Yes</span>';
                	}
                })
                ->addColumn('idMenu', static function ($row) {
                    return \Crypt::encrypt($row->id);
                })
                ->rawColumns(['is_parent', 'idMenu'])
                ->make(true);
        }

    	return view('backend.homepage.menu.index');
	}

	public function create()
	{
	    return view('backend.homepage.menu.create');
	}

	public function store(Request $request)
    {	        
        //validasi data
	    $this->validate($request, [
	        'name' => 'string|max:100|unique:menu_fronts',
	        'url' => 'nullable|string|max:100',
	        'is_parent' => 'integer',
	        'no_order' => 'integer|unique:menu_fronts',
	        'created_at' => date("Y-m-d H:i:s")
	    ]);	

	    try {
	        
	        $menu = MenuFront::create([
	            'name' => $request->name,
	            'url' => $request->url,
	            'is_parent' => $request->is_parent,
	            'no_order' => $request->no_order
	        ]);

	        if($request->is_parent == 0){
		        $submenu = SubMenuFront::create([
		            'menu_id' => $menu->id,
		            'status' => 0,
		            'no_order' => 0
		        ]);
	        }

	        self::success('Menu has been added.');
	        return redirect(route('menufront.index'));
	    } catch (\Exception $e) {
	        self::danger('Adding menu has been failed.');
	        return redirect()->back();
	    }
    }

    public function destroy($id)
    { 
    	$id = (int)\Crypt::decrypt($id);
	    $menu = MenuFront::findOrFail($id);
	    $submenu = SubMenuFront::where('menu_id', $menu->id)->delete();
	    $menu->delete();

	    self::danger('Menu has been deleted.');
	    return redirect()->back();
    }

    public function edit($id)
	{
		$id = (int)\Crypt::decrypt($id);
	    $menu = MenuFront::findOrFail($id);
	    return view('backend.homepage.menu.edit', compact('menu'));
	}

	public function update(Request $request, $id)
    {  

	    $this->validate($request, [
	        'name' => 'string|max:100',
	        'url' => 'nullable|string|max:100',
	        'url' => 'nullable|string|max:100',
	        'is_parent' => 'integer',
	        'no_order' => 'integer',
	        'updated_at' => date("Y-m-d H:i:s")
	    ]);

	    try {
	        $menu = MenuFront::findOrFail($id);

	        $menu->update([
	        	'name' => $request->name,
	            'url' => $request->url,
	            'is_parent' => $request->is_parent,
	            'no_order' => $request->no_order,
	            'updated_at' => date("Y-m-d H:i:s")
	        ]);
	        
		    self::success('Menu has been updated.');
			return redirect(route('menufront.index'));
	    } catch (\Exception $e) {
	    	self::danger('Data menu failed to update.');
	        return redirect()->back();
	    }
    }
}
