<?php

namespace App\Http\Controllers\Backend\Homepage;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\AboutUs;

class AboutusController extends Controller
{

    public function index()
	{
		$data = AboutUs::whereNull('deleted_at')->where('id', 1)->first();
		return view('backend.homepage.aboutus.index', compact('data'));
	}

	public function update(Request $request, $id)
    {  
	    $this->validate($request, [
	        'judul' => 'nullable|string',
	        'pengantar' => 'nullable|string',
	        'isi' => 'nullable|string',
	        'judul_visi' => 'nullable|string|max:255',
	        'isi_visi' => 'nullable|string',
	        'judul_misi' => 'nullable|string|max:255',
	        'isi_misi' => 'nullable|string',
	        'judul_value' => 'nullable|string|max:255',
	        'isi_value' => 'nullable|string',
	        'url_video' => 'nullable|string',
	        'updated_at' => date("Y-m-d H:i:s")
	    ]);

	    try {
	        $data = AboutUs::findOrFail($id);

	        $data->update([
	        	'judul' => $request->judul,
	            'pengantar' => $request->pengantar,
	            'isi' => $request->isi,
	            'judul_visi' => $request->judul_visi,
	            'isi_visi' => $request->isi_visi,
	            'judul_misi' => $request->judul_misi,
	            'isi_misi' => $request->isi_misi,
	            'judul_value' => $request->judul_value,
	            'isi_value' => $request->isi_value,
	            'url_video' => $request->url_value,
	            'updated_at' => date("Y-m-d H:i:s")
	        ]);
	        
		    self::success('Data about us has been updated.');
			return redirect(route('aboutus.index'));
	    } catch (\Exception $e) {
	    	self::danger('Data about us failed to update.');
	        return redirect()->back();
	    }
    }
}
