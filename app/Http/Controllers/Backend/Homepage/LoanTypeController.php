<?php

namespace App\Http\Controllers\Backend\Homepage;

use Auth;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use App\Models\LoanType;
use File;
use Intervention\Image\ImageManagerStatic as Image;

class LoanTypeController extends Controller
{

    public function index()
	{
		if(request()->ajax())
        {
            $data = LoanType::whereNull('deleted_at')->orderBy('id', 'ASC')->get();

            return datatables()->of($data)
                ->addColumn('image', static function ($row) {
                	$url= asset('uploads/loan_type/'.$row->image);

                	if(!empty($row->image)){
        				return '<img src="'.$url.'" alt="'.$row->name.'" width="100%" style="max-width: 150px;"/>';
                	}else{
                		return '<img src="http://via.placeholder.com/50x50" alt="'.$row->name.'">';
                	}
                })
                ->addColumn('image_front', static function ($row) {
                	$url= asset('uploads/loan_type/'.$row->image_front);

                	if(!empty($row->image_front)){
        				return '<img src="'.$url.'" alt="'.$row->name.'" width="100%" style="max-width: 150px;"/>';
                	}else{
                		return '<img src="http://via.placeholder.com/50x50" alt="'.$row->name.'">';
                	}
                })
                ->addColumn('idLoanType', static function ($row) {
                    return \Crypt::encrypt($row->id);
                })
                ->rawColumns(['image', 'image_front', 'idLoanType'])
                ->make(true);
        }

    	return view('backend.homepage.loan_type.index');
	}

	public function create()
	{
	    return view('backend.homepage.loan_type.create');
	}

	
	public function saveImage($name, $foto)
    {     

	    $images = Str::slug($name) . time() . '.' . $foto->getClientOriginalExtension();
	    $path = public_path('uploads/loan_type');

	    if (!File::isDirectory($path)) {
	        File::makeDirectory($path, 0777, true, true);
	    }	

	    Image::make($foto)->save($path . '/' . $images);
	    return $images;
    }

	public function store(Request $request)
    {	        
        //validasi data
	    $this->validate($request, [
	        'name' => 'string|max:255',
	        'image' => 'image|mimes:jpg,png,jpeg',
	        'image_front' => 'image|mimes:jpg,png,jpeg',
	        'url' => 'nullable|string|max:100',
	        'description' => 'string|nullable',
	        'no_order' => 'integer|unique:loan_types',
	        'created_at' => date("Y-m-d H:i:s")
	    ]);	

	    try {

	        $foto = null;
	        $foto_front = null;
	        if ($request->hasFile('image')) {
	            $foto = $this->saveImage($request->name, $request->file('image'));
	        }
	        if ($request->hasFile('image_front')) {
	            $foto_front = $this->saveImage($request->name, $request->file('image_front'));
	        }

	        $loanType = LoanType::create([
	            'name' => $request->name,
	            'image' => $foto,
	            'image_front' => $foto_front,
	            'url' => $request->url,
	            'description' => $request->description,
	            'no_order' => $request->no_order,
	        ]);

	        self::success('Loan type has been added.');
	        return redirect(route('loantype.index'));
	    } catch (\Exception $e) {
	        self::danger('Adding loan type has been failed.');
	        return redirect()->back();
	    }
    }

    public function destroy($id)
    { 
    	$id = (int)\Crypt::decrypt($id);
	    $loanType = LoanType::findOrFail($id);
	    File::delete(public_path('uploads/loan_type/' . $loanType->image));
	    File::delete(public_path('uploads/loan_type/' . $loanType->image_front));
	    $loanType->delete();

	    self::danger('Data has been deleted.');
	    return redirect()->back();
    }

    public function edit($id)
	{
		$id = (int)\Crypt::decrypt($id);
	    $loanType = LoanType::findOrFail($id);
	    return view('backend.homepage.loan_type.edit', compact('loanType'));
	}

	public function update(Request $request, $id)
    {  
	    $this->validate($request, [
	        'name' => 'string|max:255',
	        'image' => 'image|mimes:jpg,png,jpeg',
	        'image_front' => 'image|mimes:jpg,png,jpeg',
	        'url' => 'nullable|string|max:100',
	        'description' => 'string|nullable',
	        'no_order' => 'integer',
	        'updated_at' => date("Y-m-d H:i:s")
	    ]);

	    try {
	        $loanType = LoanType::findOrFail($id);
	        $foto = $loanType->image;
	        $foto_front = $loanType->image_front;

	        if ($request->hasFile('image')) {
	            !empty($foto) ? File::delete(public_path('uploads/loan_type/' . $foto)):null;
	            $foto = $this->saveImage($request->name, $request->file('image'));
	        }

			if ($request->hasFile('image_front')) {
	            !empty($foto_front) ? File::delete(public_path('uploads/loan_type/' . $foto_front)):null;
	            $foto_front = $this->saveImage($request->name, $request->file('image_front'));
	        }

	        $loanType->update([
	        	'name' => $request->name,
	            'image' => $foto,
	            'image_front' => $foto_front,
	            'url' => $request->url,
	            'description' => $request->description,
	            'no_order' => $request->no_order,	
	            'updated_at' => date("Y-m-d H:i:s")
	        ]);

		    self::success('Data has been updated.');
			return redirect(route('loantype.index'));
	    } catch (\Exception $e) {
	    	self::danger('Data failed to update.');
	        return redirect()->back();
	    }
    }
}
