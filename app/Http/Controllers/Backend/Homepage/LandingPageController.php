<?php

namespace App\Http\Controllers\Backend\Homepage;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\LandingPage;
use File;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Str;

class LandingPageController extends Controller
{

    public function index()
	{
		$data = LandingPage::whereNull('deleted_at')->where('id', 1)->first();
		return view('backend.homepage.landing_page.index', compact('data'));
	}

    public function saveImage($name, $foto)
    {     

	    $images = Str::slug($name) . time() . '.' . $foto->getClientOriginalExtension();
	    $path = public_path('uploads/landing_page');

	    if (!File::isDirectory($path)) {
	        File::makeDirectory($path, 0777, true, true);
	    }	

	    Image::make($foto)->save($path . '/' . $images);
	    return $images;
    }

	public function update(Request $request, $id)
    {  
	    $this->validate($request, [
            'banner' => 'image|mimes:jpg,png,jpeg',
	        'teks_1' => 'nullable|string',
	        'deskripsi_1' => 'nullable|string',
	        'teks_2' => 'nullable|string',
	        'deskripsi_2' => 'nullable|string',
	        'teks_3' => 'nullable|string',
	        'deskripsi_3' => 'nullable|string',
	        'updated_at' => date("Y-m-d H:i:s")
	    ]);

	    try {
	        $data = LandingPage::findOrFail($id);
            $foto = $data->banner;

            if ($request->hasFile('banner')) {
	            !empty($foto) ? File::delete(public_path('uploads/lamnding_page/' . $foto)):null;
	            $foto = $this->saveImage('banner_landing_page', $request->file('banner'));
	        }

	        $data->update([
	        	'banner' => $foto,
	            'teks_1' => $request->teks_1,
	            'deskripsi_1' => $request->deskripsi_1,
	            'teks_2' => $request->teks_2,
	            'deskripsi_2' => $request->deskripsi_2,
	            'teks_3' => $request->teks_3,
	            'deskripsi_3' => $request->deskripsi_3,
	            'updated_at' => date("Y-m-d H:i:s")
	        ]);
	        
		    self::success('Data landing page has been updated.');
			return redirect(route('landingpage.index'));
	    } catch (\Exception $e) {
	    	self::danger('Data landing page failed to update.');
	        return redirect()->back();
	    }
    }
}
