<?php

namespace App\Http\Controllers\Backend\Homepage;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use App\Models\ContactUs;

class ContactUsController extends Controller
{

    public function index()
	{
		if(request()->ajax())
        {

    		$data = ContactUs::whereNull('deleted_at')->orderBy('id', 'ASC')->get();

            return datatables()->of($data)
                ->addColumn('id', static function ($row) {
                    return \Crypt::encrypt($row->id);
                })
                ->rawColumns(['id'])
                ->make(true);
        }

    	return view('backend.homepage.contactus.index');
	}

    public function destroy($id)
    { 
    	$id = (int)\Crypt::decrypt($id);
	    $data = ContactUs::findOrFail($id);
	    $data->delete();

	    self::danger('Data has been deleted.');
	    return redirect()->back();
    }

    public function edit($id)
	{
		$id = (int)\Crypt::decrypt($id);
	    $data = ContactUs::findOrFail($id);
	    return view('backend.homepage.contactus.edit', compact('data'));
	}

	public function update(Request $request, $id)
    {  
	    $this->validate($request, [
	        'icon' => 'image|mimes:jpg,png,jpeg',
	        'title' => 'string|max:100',
	        'description' => 'string|nullable',
	        'updated_at' => date("Y-m-d H:i:s")
	    ]);

	    try {
	        $data = ContactUs::findOrFail($id);
            $foto = $data->icon;

	        if ($request->hasFile('icon')) {
	            !empty($foto) ? File::delete(public_path('uploads/keunggulan/' . $foto)):null;
	            $foto = $this->saveImage($request->title, $request->file('icon'));
	        }

	        $data->update([
	        	'icon' => $foto,
	            'title' => $request->title,
	            'description' => $request->description,
	            'updated_at' => date("Y-m-d H:i:s")
	        ]);
	        
		    self::success('Data has been updated.');
			return redirect(route('contactus.index'));
	    } catch (\Exception $e) {
	    	self::danger('Data failed to update.');
	        return redirect()->back();
	    }
    }
}
