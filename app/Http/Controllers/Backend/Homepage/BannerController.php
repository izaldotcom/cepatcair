<?php

namespace App\Http\Controllers\Backend\Homepage;

use Auth;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use App\Models\Banner;
use App\Models\Log;
use File;
use Storage;
use Intervention\Image\ImageManagerStatic as Image;

class BannerController extends Controller
{

    public function index()
	{
		if(request()->ajax())
        {
            $data = Banner::whereNull('deleted_at')->orderBy('id', 'ASC')->get();

            return datatables()->of($data)
                ->addColumn('image', static function ($row) {
                	//$url= asset('storage/image/banner/'.$row->photo);
                	$url = app('file.helper')->getUrlStorage($row->photo, 'image_banner');
                	if(!empty($row->photo)){
        				return '<img src="'.$url.'" alt="'.$row->banner_name.'" width="100%" style="max-width: 150px;"/>';
                	}else{
                		return '<img src="http://via.placeholder.com/50x50" alt="'.$row->banner_name.'">';
                	}
                })
                ->addColumn('status', static function ($row) {
                    if($row->status == 0){
                        return '<span class="badge bg-warning">-</span>';
                    }elseif($row->status == 1){
                        return '<span class="badge bg-success">Active</span>';
                    }else{
                        return '<span class="badge bg-danger">Inactive</span>';
                    }
                })
                ->addColumn('type', static function ($row) {
                    if($row->type == 1){
                        return 'Promotion';
                    }elseif($row->type == 2){
                        return 'Info';
                    }elseif($row->type == 3){
                    	return 'Discount';
                    }else{
                        return '-';
                    }
                })
                ->addColumn('idBanner', static function ($row) {
                    return \Crypt::encrypt($row->id);
                })
                ->rawColumns(['image', 'status', 'type', 'idBanner'])
                ->make(true);
        }

    	return view('master.banner.index');
	}

	public function create()
	{
	    return view('master.banner.create');
	}

	public function saveFile($name, $foto)
    {        
	    $images = Str::slug($name) . time() . '.' . $foto->getClientOriginalExtension();
	    $storage_image = (in_array(config('app.env'), ['staging', 'production'])) ? 's3' : 'image_banner';
	    Storage::disk($storage_image)->put($images, File::get($foto));

	    // $path = public_path('storage/image/banner');

	    // if (!File::isDirectory($path)) {
	    //     File::makeDirectory($path, 0777, true, true);
	    // }	

	    // Image::make($foto)->save($path . '/' . $images);
	    return $images;
    }

	public function store(Request $request)
    {	        
        //validasi data
	    $this->validate($request, [
	        'banner_name' => 'string|max:191',
	        'photo' => 'image|mimes:jpg,png,jpeg',
	        'status' => 'integer',
	        'type' => 'integer',
	        'created_at' => date("Y-m-d H:i:s")
	    ]);	

	    try {

	        $foto = null;
	        if ($request->hasFile('photo')) {
	            $foto = $this->saveFile($request->name, $request->file('photo'));
	        }

	        $banner = Banner::create([
	            'banner_name' => $request->banner_name,
	            'status' => $request->status,
	            'type' => $request->type,
	            'photo' => $foto,
	        ]);

	        $params = [
	        	'object' => $request->banner_name,
	        	'details' => json_encode($banner),
	        	'details_changed' => null,
                'name' => Auth::user()->name,
                'type' => Log::TYPE_BANNER,
                'method' => Log::METHOD_CREATED,
                'status' => Log::STATUS_SUCCESS,
            ];

	        app('log.helper')->createLog($params);

	        self::success('Banner has been added.');
	        return redirect(route('banner.index'));
	    } catch (\Exception $e) {

	    	$params = [
	        	'object' => $request->banner_name,
	        	'details' => json_encode($banner),
	        	'details_changed' => null,
                'name' => Auth::user()->name,
                'type' => Log::TYPE_BANNER,
                'method' => Log::METHOD_CREATED,
                'status' => Log::STATUS_ERROR,
            ];

	        app('log.helper')->createLog($params);
	        
	        self::danger('Adding banner has been failed.');
	        return redirect()->back();
	    }
    }

    public function destroy($id)
    { 
    	$id = (int)\Crypt::decrypt($id);
	    $banner = Banner::findOrFail($id);
        $storage_image = (in_array(config('app.env'), ['staging', 'production'])) ? 's3' : 'image_banner';
        Storage::disk($storage_image)->delete($banner->photo);
	    // $banner = Banner::where('id_menu', $banner->id)->delete();
	    //$permission = Permission::where('menu_name', str_replace(' ', '', strtolower($banner->name)))->delete();
	    $params = [
        	'object' => $banner->banner_name,
        	'details' => json_encode($banner),
        	'details_changed' => null,
            'name' => Auth::user()->name,
            'type' => Log::TYPE_BANNER,
            'method' => Log::METHOD_DELETED,
            'status' => Log::STATUS_SUCCESS,
        ];

        app('log.helper')->createLog($params);

	    $banner->delete();

	    self::danger('Banner has been deleted.');
	    return redirect()->back();
    }

    public function edit($id)
	{
		$id = (int)\Crypt::decrypt($id);
	    $banner = Banner::findOrFail($id);
	    $url = app('file.helper')->getUrlStorage($banner->photo, 'image_banner');
	    return view('master.banner.edit', compact('banner', 'url'));
	}

	public function update(Request $request, $id)
    {  
	    $this->validate($request, [
	        'banner_name' => 'string|max:191',
	        'photo' => 'image|mimes:jpg,png,jpeg',
	        'status' => 'integer',
	        'type' => 'integer',
	        'updated_at' => date("Y-m-d H:i:s")
	    ]);

	    try {
	        $banner = Banner::findOrFail($id);
	        $foto = $banner->photo;
	        $storage_image = (in_array(config('app.env'), ['staging', 'production'])) ? 's3' : 'image_banner';

	        if ($request->hasFile('photo')) {
	            Storage::disk($storage_image)->delete($foto);
	            //!empty($foto) ? File::delete(public_path('storage/image/banner/' . $foto)):null;
	            $foto = $this->saveFile($request->name, $request->file('photo'));
	            $gambar = $foto;
	        }else{
	        	$gambar = null;
	        }
	        
	        $requestChanged = [
	        	'banner_name' => $request->banner_name,
	        	'photo' => $gambar,
	        	'status' => $request->status,
	        	'type' => $request->type,
	        ];

	        $params = [
	        	'object' => $banner->banner_name,
	        	'details' => json_encode($banner),
	        	'details_changed' => json_encode($requestChanged),
                'name' => Auth::user()->name,
                'type' => Log::TYPE_BANNER,
                'method' => Log::METHOD_UPDATED,
                'status' => Log::STATUS_SUCCESS,
            ];

	        app('log.helper')->createLog($params);

	        $banner->update([
	        	'banner_name' => $request->banner_name,
	            'status' => $request->status,
	            'type' => $request->type,
	            'photo' => $foto,	
	            'updated_at' => date("Y-m-d H:i:s")
	        ]);

	        
		    self::success('Banner has been updated.');
			return redirect(route('banner.index'));
	    } catch (\Exception $e) {

	    	$requestChanged = [
	        	'banner_name' => $request->banner_name,
	        	'photo' => $gambar,
	        	'status' => $request->status,
	        	'type' => $request->type,
	        ];

	    	$params = [
	        	'object' => $banner->banner_name,
	        	'details' => json_encode($banner),
	        	'details_changed' => json_encode($requestChanged),
                'name' => Auth::user()->name,
                'type' => Log::TYPE_BANNER,
                'method' => Log::METHOD_UPDATED,
                'status' => Log::STATUS_ERROR,
            ];

	        app('log.helper')->createLog($params);

	    	self::danger('Banner failed to update.');
	        return redirect()->back();
	    }
    }
}
