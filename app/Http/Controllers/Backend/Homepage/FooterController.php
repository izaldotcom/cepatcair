<?php

namespace App\Http\Controllers\Backend\Homepage;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Footer;
use App\Models\SubMenu;
use App\Models\Permission;

class FooterController extends Controller
{

    public function index()
	{
		$data = Footer::whereNull('deleted_at')->where('id', 1)->first();
		return view('backend.homepage.footer.index', compact('data'));
	}

	public function create()
	{
	    return view('backend.utilitas.menu.create');
	}

	public function update(Request $request, $id)
    {  
	    $this->validate($request, [
	        'alamat' => 'nullable|string',
	        'no_telp' => 'nullable|string|max:100',
	        'email' => 'nullable|string|max:100',
	        'jam_operasional' => 'nullable|string',
	        'url_fb' => 'nullable|string',
	        'url_twitter' => 'nullable|string',
	        'url_ig' => 'nullable|string',
	        'deskripsi' => 'nullable|string',
	        'updated_at' => date("Y-m-d H:i:s")
	    ]);

	    try {
	        $footer = Footer::findOrFail($id);

	        $footer->update([
	        	'alamat' => $request->alamat,
	            'no_telp' => $request->no_telp,
	            'email' => $request->email,
	            'jam_operasional' => $request->jam_operasional,
	            'url_fb' => $request->url_fb,
	            'url_twitter' => $request->url_twitter,
	            'url_ig' => $request->url_ig,
	            'deskripsi' => $request->deskripsi,
	            'updated_at' => date("Y-m-d H:i:s")
	        ]);
	        
		    self::success('Data footer has been updated.');
			return redirect(route('footer.index'));
	    } catch (\Exception $e) {
	    	self::danger('Data footer failed to update.');
	        return redirect()->back();
	    }
    }
}
