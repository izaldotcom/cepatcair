<?php

namespace App\Http\Controllers\Backend\Utilitas;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Role;
use App\Models\User;
use App\Models\SubMenu;
use Spatie\Permission\Models\Permission;
use DB;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {   
        if(request()->ajax())
        {

            $data = User::whereNull('deleted_at')->orderBy('created_at', 'DESC')->get();

            return datatables()->of($data)
                ->addColumn('role', static function ($row) {
                    $roleName = '';
                    foreach ($row->getRoleNames() as $role) {
                        $roleName .= $role;
                    }
                    return '<label for="" class="badge badge-info">'.$roleName.'</label>';
                })
                ->addColumn('status', static function ($row) {
                    if($row->status == 'aktif'){
                        return '<label class="badge badge-success">Aktif</label>';
                    }else{
                        return '<label class="badge badge-warning">Suspend</label>';
                    }
                })
                ->addColumn('idUser', static function ($row) {
                    return \Crypt::encrypt($row->id);
                })
                ->rawColumns(['role', 'status', 'idMenu'])
                ->make(true);
        }

        return view('backend.utilitas.user.index');
    }

    public function create()
    {
        $role = Role::orderBy('name', 'ASC')->get();
        return view('backend.utilitas.user.create', compact('role'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:100',
            'email' => 'required|string|unique:users',
            'password' => 'required|min:6',
            'role' => 'required|string|exists:roles,name'
        ]);

        $user = User::firstOrCreate([
            'email' => $request->email
        ], [
            'name' => $request->name,
            'password' => bcrypt($request->password),
            'status' => 'suspend'
        ]);

        $user->assignRole($request->role);

        self::success('Data user berhasil ditambahkan.');
        return redirect(route('user.index'));
    }

    public function edit($id)
    {
        $id = (int)\Crypt::decrypt($id);
        $user = User::findOrFail($id);
        return view('backend.utilitas.user.edit', compact('user'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|string|max:100',
            'email' => 'required|string|exists:users,email',
            'password' => 'nullable|min:6',
            'status' => 'required|string',
        ]);

        $user = User::findOrFail($id);
        $password = !empty($request->password) ? bcrypt($request->password):$user->password;
        $user->update([
            'name' => $request->name,
            'password' => $password,
            'status' => $request->status
        ]);
        self::success('Data user berhasil diubah.');
        return redirect(route('user.index'));
    }

    public function destroy($id)
    {
        $id = (int)\Crypt::decrypt($id);
        $user = User::where('id', $id)->delete();
        self::danger('Data user berhasil dihapus.');
        return redirect()->back();
    }

    public function roles(Request $request, $id)
    {
        $id = (int)\Crypt::decrypt($id);
        $user = User::findOrFail($id);
        $roles = Role::all()->pluck('name');
        return view('backend.utilitas.user.roles', compact('user', 'roles'));
    }

    public function setRole(Request $request, $id)
    {
        $this->validate($request, [
            'role' => 'required'
        ]);
        
        $user = User::findOrFail($id);
        $user->syncRoles($request->role);

        self::success('Data role berhasil diset.');
        return redirect()->back();
    }

    public function rolePermission(Request $request)
    {
        $submenu = SubMenu::with('menu')->orderBy('menu_id', 'ASC')->orderBy('no_order', 'ASC')->get();
        $role = $request->get('role');

        $permissions = null;
        $hasPermission = null;
        
        $roles = Role::all()->pluck('name');
        
        if (!empty($role)) {
            $getRole = Role::findByName($role);
            
            $hasPermission = DB::table('role_has_permissions')
                ->select('permissions.name')
                ->join('permissions', 'role_has_permissions.permission_id', '=', 'permissions.id')
                ->where('role_id', $getRole->id)->get()->pluck('name')->all();
            
            $permissions = Permission::all()->pluck('name');
        }
        return view('backend.utilitas.user.role_permission', compact('roles', 'permissions', 'hasPermission', 'submenu'));
    }

    public function addPermission(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|unique:permissions'
        ]);

        $explodeMenu = explode('-', $request->name);

        $permission = Permission::firstOrCreate([
            'name' => $request->name,
            'menu_name' => $explodeMenu[0]
        ]);
        self::success('Data permission berhasil ditambahkan.');
        return redirect()->back();
    }

    public function setRolePermission(Request $request, $role)
    {
        $role = Role::findByName($role);
        $role->syncPermissions($request->permission);
        self::success('Data permission berhasil disimpan.');
        return redirect()->back();
    }

}