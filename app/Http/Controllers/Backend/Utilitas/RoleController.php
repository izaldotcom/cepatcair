<?php

namespace App\Http\Controllers\Backend\Utilitas;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use App\Http\Controllers\Controller;

class RoleController extends Controller
{
	public function index(){
		$role = Role::orderBy('created_at', 'DESC')->paginate(10);
		return view('backend.utilitas.role.index', compact('role'));
	}
	
    public function store(Request $request)
	{
	    $this->validate($request, [
	        'name' => 'required|string|max:50'
	    ]);

	    $role = Role::firstOrCreate(['name' => $request->name]);
	    self::success('Data berhasil disimpan.');
	    return redirect()->back();
	}

	public function destroy($id)
	{
	    $role = Role::findOrFail($id);
	    $role->delete();
	    self::danger('Data berhasil dihapus.');
	    return redirect()->back();
	}
}
