<?php

namespace App\Http\Controllers\Backend\Utilitas;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Menu;
use App\Models\SubMenu;
use App\Models\Permission;

class MenuController extends Controller
{

    public function index()
	{
		if(request()->ajax())
        {

    		$data = Menu::whereNull('deleted_at')->orderBy('no_order', 'ASC')->get();

            return datatables()->of($data)
                ->addColumn('is_parent', static function ($row) {
                	if($row->is_parent == 0){
                		return '<span class="badge bg-danger">No</span>';
                	}else{
                		return '<span class="badge bg-success">Yes</span>';
                	}
                })
                ->addColumn('idMenu', static function ($row) {
                    return \Crypt::encrypt($row->id);
                })
                ->rawColumns(['is_parent', 'idMenu'])
                ->make(true);
        }

    	return view('backend.utilitas.menu.index');
	}

	public function create()
	{
	    return view('backend.utilitas.menu.create');
	}

	public function store(Request $request)
    {	        
        //validasi data
	    $this->validate($request, [
	        'name' => 'string|max:100|unique:menus',
	        'icon' => 'nullable|string|max:100',
	        'url' => 'nullable|string|max:100',
	        'description' => 'nullable|string',
	        'is_parent' => 'integer',
	        'no_order' => 'integer|unique:menus',
	        'created_at' => date("Y-m-d H:i:s")
	    ]);	

	    try {
	        
	        $menu = Menu::create([
	            'name' => $request->name,
	            'icon' => $request->icon,
	            'url' => $request->url,
	            'description' => $request->description,
	            'is_parent' => $request->is_parent,
	            'no_order' => $request->no_order
	        ]);

	        if($request->is_parent == 0){
		        $submenu = SubMenu::create([
		            'menu_id' => $menu->id,
		            'status' => 0,
		            'no_order' => 0
		        ]);
	        }

	        self::success('Menu has been added.');
	        return redirect(route('menu.index'));
	    } catch (\Exception $e) {
	        self::danger('Adding menu has been failed.');
	        return redirect()->back();
	    }
    }

    public function destroy($id)
    { 
    	$id = (int)\Crypt::decrypt($id);
	    $menu = Menu::findOrFail($id);
	    $submenu = SubMenu::where('menu_id', $menu->id)->delete();
	    $permission = Permission::where('menu_name', str_replace(' ', '', strtolower($menu->name)))->delete();
	    $menu->delete();

	    self::danger('Menu has been deleted.');
	    return redirect()->back();
    }

    public function edit($id)
	{
		$id = (int)\Crypt::decrypt($id);
	    $menu = Menu::findOrFail($id);
	    return view('backend.utilitas.menu.edit', compact('menu'));
	}

	public function update(Request $request, $id)
    {  
	    $this->validate($request, [
	        'name' => 'string|max:100',
	        'icon' => 'nullable|string|max:100',
	        'url' => 'nullable|string|max:100',
	        'description' => 'nullable|string',
	        'is_parent' => 'integer',
	        'no_order' => 'integer',
	        'updated_at' => date("Y-m-d H:i:s")
	    ]);

	    try {
	        $menu = Menu::findOrFail($id);

	        $menu->update([
	        	'name' => $request->name,
	            'icon' => $request->icon,
	            'url' => $request->url,
	            'description' => $request->description,
	            'is_parent' => $request->is_parent,
	            'no_order' => $request->no_order,
	            'updated_at' => date("Y-m-d H:i:s")
	        ]);
	        
		    self::success('Menu has been updated.');
			return redirect(route('menu.index'));
	    } catch (\Exception $e) {
	    	self::danger('Data menu failed to update.');
	        return redirect()->back();
	    }
    }
}
