<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use App\Models\MenuFront;
use App\Models\SubMenuFront;
use App\Models\LoanType;
use App\Models\LandingPage;
use App\Models\Footer;
use App\Models\ContactUs;
use Illuminate\Support\Facades\Validator;
class ContactUsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('web');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $menu = MenuFront::orderBy('no_order', 'ASC')->get();
        $menuMobile = MenuFront::orderBy('no_order', 'ASC')->get();
        $submenu = SubMenuFront::orderBy('no_order', 'ASC')->get();
        $submenuMobile = SubMenuFront::orderBy('no_order', 'ASC')->get();
        $loanType = LoanType::orderBy('no_order', 'ASC')->limit(5)->get();
        $loanTypeFooter = LoanType::orderBy('no_order', 'ASC')->get();
        $landingPage = LandingPage::whereNull('deleted_at')->where('id', 1)->first();
        $footer = Footer::whereNull('deleted_at')->where('id', 1)->first();

        return view('contactus', compact(
            'menu', 
            'submenu', 
            'menuMobile', 
            'submenuMobile', 
            'loanType', 
            'loanTypeFooter', 
            'landingPage', 
            'footer'
        ));
    }

    public function send(Request $request)
    {
        try {
            
            //validasi data
            $rules = [
                'name' => 'string|max:255',
                'email' => 'string|max:255',
                'message' => 'string',
                'created_at' => date("Y-m-d H:i:s")
            ];

            $validated = Validator::make($request->all(), $rules);
            
            if($validated->fails())
            {
                return response()->json([
                    'status' => 'error', 
                    'messages' => 'Kesalahan input data', 
                    'errors' => $validated->errors()
                ]);
            }
            
            $data = ContactUs::create([
                'name' => $request->name,
                'email' => $request->email,
                'message' => $request->message
            ]);

            if (!$data) {
                return response()->json([
                    'status'=>'error',
                    'message'=>'Gagal mengirimkan data.'
                ], 500);
            }else{
                return response()->json([
                    'status' => 'success',
                    'data' => $data
                ]);
            }

        } catch (\Exception $e) {
	    	return response()->json([
                'status'=>'error',
                'message'=>'Gagal mengirimkan data.'
            ], 500);
	    }
        
    }
    
}
