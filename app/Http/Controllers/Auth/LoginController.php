<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Models\User;
use App\Models\Menu;
use App\Models\SubMenu;
use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::BACKEND_HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function index()
    {
        return view('auth.login-admin');
    }

    public function processLogin(Request $request) 
    {
        $user = User::where('email', $request->email)->first();
        $menu = Menu::orderBy('no_order', 'ASC')->get();
        $submenu= SubMenu::with('menu')->where('status', 1)->orderBy('no_order', 'ASC')->get();

        $query = "
                SELECT b.menu_name FROM 
                role_has_permissions a
                JOIN permissions b ON a.permission_id = b.id
                LEFT JOIN roles c ON a.role_id = c.id
                LEFT JOIN model_has_roles d ON c.id = d.role_id
                LEFT JOIN users e ON e.id = d.model_id
                WHERE e.email = '".$user->email."'
                GROUP BY b.menu_name
                ";

        $userMenu = DB::select(DB::raw($query));

        if($user){
            if (auth()->attempt(['email' => $request->email, 'password' => $request->password]))
            {
                Session::put('sess_menu', $menu);
                Session::put('sess_submenu', $submenu);
                Session::put('sess_usermenu', $userMenu);
                Session::put('sess_user', $user);
                self::success('Successfully login.');
                return redirect()->intended('/backend/home');
            }else{
                self::danger('Wrong email or password.');
                return redirect()->intended('/login-system');    
            }
        }else{
            self::danger('User is not existed.');
            return redirect()->intended('/login-system');
        }
    }

    public function logout(Request $request) 
    {
        Session::flush();
        Session::regenerate();
        Auth::logout();
        self::danger('Successfully logout from system');
        return redirect('/login-system');
    }
}
