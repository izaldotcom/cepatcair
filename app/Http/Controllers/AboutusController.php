<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use App\Models\MenuFront;
use App\Models\SubMenuFront;
use App\Models\LoanType;
use App\Models\LandingPage;
use App\Models\Footer;
use App\Models\Keunggulan;
use App\Models\Callback;
use App\Models\AboutUs;

class AboutusController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('web');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $menu = MenuFront::orderBy('no_order', 'ASC')->get();
        $menuMobile = MenuFront::orderBy('no_order', 'ASC')->get();
        $submenu = SubMenuFront::orderBy('no_order', 'ASC')->get();
        $submenuMobile = SubMenuFront::orderBy('no_order', 'ASC')->get();
        $loanType = LoanType::orderBy('no_order', 'ASC')->limit(5)->get();
        $loanTypeFooter = LoanType::orderBy('no_order', 'ASC')->get();
        $landingPage = LandingPage::whereNull('deleted_at')->where('id', 1)->first();
        $footer = Footer::whereNull('deleted_at')->where('id', 1)->first();
        $aboutus = AboutUs::whereNull('deleted_at')->where('id', 1)->first();
        $keunggulan = Keunggulan::orderBy('id', 'ASC')->get();
        $layanan = LoanType::orderBy('no_order', 'ASC')->get();

        return view('aboutus', compact(
            'menu', 
            'submenu', 
            'menuMobile', 
            'submenuMobile', 
            'loanType', 
            'loanTypeFooter', 
            'landingPage', 
            'footer', 
            'aboutus', 
            'keunggulan',
            'layanan'
        ));
    }
    
}
