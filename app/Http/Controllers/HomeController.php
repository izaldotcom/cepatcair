<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use App\Models\MenuFront;
use App\Models\SubMenuFront;
use App\Models\LoanType;
use App\Models\LandingPage;
use App\Models\Footer;
use App\Models\Keunggulan;
use App\Models\Callback;
use Illuminate\Support\Facades\Validator;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('web');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $menu = MenuFront::orderBy('no_order', 'ASC')->get();
        $menuMobile = MenuFront::orderBy('no_order', 'ASC')->get();
        $submenu = SubMenuFront::orderBy('no_order', 'ASC')->get();
        $submenuMobile = SubMenuFront::orderBy('no_order', 'ASC')->get();
        $loanType = LoanType::orderBy('no_order', 'ASC')->limit(5)->get();
        $loanTypeFooter = LoanType::orderBy('no_order', 'ASC')->get();
        $landingPage = LandingPage::whereNull('deleted_at')->where('id', 1)->first();
        $footer = Footer::whereNull('deleted_at')->where('id', 1)->first();
        $keunggulan = Keunggulan::orderBy('id', 'ASC')->get();
        $layanan = LoanType::orderBy('no_order', 'ASC')->get();

        return view('beranda', compact(
            'menu', 
            'submenu', 
            'menuMobile', 
            'submenuMobile', 
            'loanType', 
            'loanTypeFooter', 
            'landingPage', 
            'footer', 
            'keunggulan',
            'layanan'
        ));
    }

    public function sendCallback(Request $request)
    {
        try {
            
            //validasi data
            $rules = [
                'name' => 'string|nullable',
                'email' => 'string|nullable',
                'phone' => 'string|nullable',
                'service' => 'string|nullable',
                'created_at' => date("Y-m-d H:i:s")
            ];

            $validated = Validator::make($request->all(), $rules);
            
            if($validated->fails())
            {
                return response()->json([
                    'status' => 'error', 
                    'messages' => 'Kesalahan input data', 
                    'errors' => $validated->errors()
                ]);
            }
            
            $data = Callback::create([
                'name' => $request->name,
                'email' => $request->email,
                'phone' => $request->phone,
                'service' => $request->service,
            ]);

            if (!$data) {
                return response()->json([
                    'status'=>'error',
                    'message'=>'Gagal mengirimkan data.'
                ], 500);
            }else{
                return response()->json([
                    'status' => 'success',
                    'data' => $data
                ]);
            }

        } catch (\Exception $e) {
	    	return response()->json([
                'status'=>'error',
                'message'=>'Gagal mengirimkan data.'
            ], 500);
	    }
        
    }
}
