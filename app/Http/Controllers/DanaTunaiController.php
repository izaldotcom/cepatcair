<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use App\Models\MenuFront;
use App\Models\SubMenuFront;
use App\Models\LoanType;
use App\Models\LandingPage;
use App\Models\Footer;
use App\Models\TrDanaTunai;
use Illuminate\Support\Facades\Validator;
class DanaTunaiController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('web');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function indexJaminanRumah()
    {
        $menu = MenuFront::orderBy('no_order', 'ASC')->get();
        $menuMobile = MenuFront::orderBy('no_order', 'ASC')->get();
        $submenu = SubMenuFront::orderBy('no_order', 'ASC')->get();
        $submenuMobile = SubMenuFront::orderBy('no_order', 'ASC')->get();
        $loanType = LoanType::orderBy('no_order', 'ASC')->limit(5)->get();
        $loanTypeFooter = LoanType::orderBy('no_order', 'ASC')->get();
        $landingPage = LandingPage::whereNull('deleted_at')->where('id', 1)->first();
        $footer = Footer::whereNull('deleted_at')->where('id', 1)->first();
        $layanan = LoanType::orderBy('no_order', 'ASC')->get();

        return view('dana-tunai.jaminan-rumah', compact(
            'menu', 
            'submenu', 
            'menuMobile', 
            'submenuMobile', 
            'loanType', 
            'loanTypeFooter', 
            'landingPage', 
            'footer',
            'layanan'
        ));
    }

    public function indexJaminanMotor()
    {
        $menu = MenuFront::orderBy('no_order', 'ASC')->get();
        $menuMobile = MenuFront::orderBy('no_order', 'ASC')->get();
        $submenu = SubMenuFront::orderBy('no_order', 'ASC')->get();
        $submenuMobile = SubMenuFront::orderBy('no_order', 'ASC')->get();
        $loanType = LoanType::orderBy('no_order', 'ASC')->limit(5)->get();
        $loanTypeFooter = LoanType::orderBy('no_order', 'ASC')->get();
        $landingPage = LandingPage::whereNull('deleted_at')->where('id', 1)->first();
        $footer = Footer::whereNull('deleted_at')->where('id', 1)->first();
        $layanan = LoanType::orderBy('no_order', 'ASC')->get();

        return view('dana-tunai.jaminan-motor', compact(
            'menu', 
            'submenu', 
            'menuMobile', 
            'submenuMobile', 
            'loanType', 
            'loanTypeFooter', 
            'landingPage', 
            'footer',
            'layanan'
        ));
    }

    public function indexJaminanMobil()
    {
        $menu = MenuFront::orderBy('no_order', 'ASC')->get();
        $menuMobile = MenuFront::orderBy('no_order', 'ASC')->get();
        $submenu = SubMenuFront::orderBy('no_order', 'ASC')->get();
        $submenuMobile = SubMenuFront::orderBy('no_order', 'ASC')->get();
        $loanType = LoanType::orderBy('no_order', 'ASC')->limit(5)->get();
        $loanTypeFooter = LoanType::orderBy('no_order', 'ASC')->get();
        $landingPage = LandingPage::whereNull('deleted_at')->where('id', 1)->first();
        $footer = Footer::whereNull('deleted_at')->where('id', 1)->first();
        $layanan = LoanType::orderBy('no_order', 'ASC')->get();

        return view('dana-tunai.jaminan-mobil', compact(
            'menu', 
            'submenu', 
            'menuMobile', 
            'submenuMobile', 
            'loanType', 
            'loanTypeFooter', 
            'landingPage', 
            'footer',
            'layanan'
        ));
    }

    public function saveDataJaminanRumah(Request $request)
    {

        try {
            //validasi data
            $rules = [
                'nama_lengkap' => 'string|max:255',
                'no_telp' => 'string|max:255|unique:tr_dana_tunai',
                'email' => 'string|max:255|nullable',
                'alamat' => 'string',
                'status_bpkb' => 'integer',
                'tipe' => 'integer',
                'pemilik_jaminan' => 'string|nullable',
                'kode_referal' => 'string|nullable',
                'created_at' => date("Y-m-d H:i:s")
            ];
    
            $validated = Validator::make($request->all(), $rules);
            
            if($validated->fails())
            {
                return response()->json([
                    'status' => 'error', 
                    'messages' => 'Kesalahan input data / Duplikasi data', 
                    'errors' => $validated->errors()
                ]);
            }
    
            $data = TrDanaTunai::create([
                'nama_lengkap' => $request->nama_lengkap,
                'no_telp' => $request->no_telp,
                'email' => $request->email,
                'alamat' => $request->alamat,
                'status_bpkb' => $request->status_bpkb,
                'tipe' => TrDanaTunai::RUMAH,
                'pemilik_jaminan' => !is_null($request->pemilik_jaminan) ? $request->pemilik_jaminan : $request->nama_lengkap,
                'kode_referal' => $request->kode_referal
            ]);
    
            if (!$data) {
                return response()->json([
                    'status'=>'error',
                    'message'=>'Gagal mengirimkan data.'
                ], 500);
            }else{
                return response()->json([
                    'status' => 'success',
                    'data' => $data
                ]);
            }
            
        
        } catch (\Exception $e) {
            return response()->json([
                'status'=>'error',
                'message'=>'Gagal mengirimkan data.'
            ], 500);
	    }

    }

    public function saveDataJaminanMotor(Request $request)
    {
        
        try {
            //validasi data
            $rules = [
                'nama_lengkap' => 'string|max:255',
                'no_telp' => 'string|max:255|unique:tr_dana_tunai',
                'email' => 'string|max:255|nullable',
                'alamat' => 'string',
                'status_bpkb' => 'integer',
                'tipe' => 'integer',
                'pemilik_jaminan' => 'string|nullable',
                'kode_referal' => 'string|nullable',
                'created_at' => date("Y-m-d H:i:s")
            ];
    
            $validated = Validator::make($request->all(), $rules);
            
            if($validated->fails())
            {
                return response()->json([
                    'status' => 'error', 
                    'messages' => 'Kesalahan input data / Duplikasi data', 
                    'errors' => $validated->errors()
                ]);
            }
    
            $data = TrDanaTunai::create([
                'nama_lengkap' => $request->nama_lengkap,
                'no_telp' => $request->no_telp,
                'email' => $request->email,
                'alamat' => $request->alamat,
                'status_bpkb' => $request->status_bpkb,
                'tipe' => TrDanaTunai::MOTOR,
                'pemilik_jaminan' => !is_null($request->pemilik_jaminan) ? $request->pemilik_jaminan : $request->nama_lengkap,
                'kode_referal' => $request->kode_referal
            ]);
    
            if (!$data) {
                return response()->json([
                    'status'=>'error',
                    'message'=>'Gagal mengirimkan data.'
                ], 500);
            }else{
                return response()->json([
                    'status' => 'success',
                    'data' => $data
                ]);
            }
            
        
        } catch (\Exception $e) {
            return response()->json([
                'status'=>'error',
                'message'=>'Gagal mengirimkan data.'
            ], 500);
	    }

    }

    public function saveDataJaminanMobil(Request $request)
    {

        try {
            //validasi data
            $rules = [
                'nama_lengkap' => 'string|max:255',
                'no_telp' => 'string|max:255|unique:tr_dana_tunai',
                'email' => 'string|max:255|nullable',
                'alamat' => 'string',
                'status_bpkb' => 'integer',
                'tipe' => 'integer',
                'pemilik_jaminan' => 'string|nullable',
                'kode_referal' => 'string|nullable',
                'created_at' => date("Y-m-d H:i:s")
            ];
    
            $validated = Validator::make($request->all(), $rules);
            
            if($validated->fails())
            {
                return response()->json([
                    'status' => 'error', 
                    'messages' => 'Kesalahan input data / Duplikasi data', 
                    'errors' => $validated->errors()
                ]);
            }
    
            $data = TrDanaTunai::create([
                'nama_lengkap' => $request->nama_lengkap,
                'no_telp' => $request->no_telp,
                'email' => $request->email,
                'alamat' => $request->alamat,
                'status_bpkb' => $request->status_bpkb,
                'tipe' => TrDanaTunai::MOBIL,
                'pemilik_jaminan' => !is_null($request->pemilik_jaminan) ? $request->pemilik_jaminan : $request->nama_lengkap,
                'kode_referal' => $request->kode_referal
            ]);
    
            if (!$data) {
                return response()->json([
                    'status'=>'error',
                    'message'=>'Gagal mengirimkan data.'
                ], 500);
            }else{
                return response()->json([
                    'status' => 'success',
                    'data' => $data
                ]);
            }
            
        
        } catch (\Exception $e) {
            return response()->json([
                'status'=>'error',
                'message'=>'Gagal mengirimkan data.'
            ], 500);
	    }

    }
    
}
