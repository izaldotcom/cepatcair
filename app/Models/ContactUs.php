<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Contactu
 * 
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $message
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 *
 * @package App\Models
 */
class ContactUs extends Model
{
	use SoftDeletes;
	protected $table = 'contactus';
	protected $primaryKey = 'id';
	
	protected $fillable = [
		'name',
		'email',
		'message'
	];
}
