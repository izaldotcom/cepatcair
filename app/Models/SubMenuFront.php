<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class User
 * 
 * @property int $id
 * @property string $name
 * @property string $email
 * @property Carbon|null $email_verified_at
 * @property string $password
 * @property string|null $remember_token
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 * 
 * @property Collection|Administrator[] $administrators
 * @property Collection|Customer[] $customers
 *
 * @package App\Models
 */
class SubMenuFront extends Model
{
	protected $table = 'submenu_fronts';
	protected $primaryKey = 'id';

	protected $fillable = [
		'menu_id',
		'name',
		'url',
		'status',
		'no_order',
	];

	public function menu()
	{	
	    return $this->belongsTo("App\Models\MenuFront", "menu_id");
	}

}
