<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class User
 * 
 * @property int $id
 * @property string $name
 * @property string $email
 * @property Carbon|null $email_verified_at
 * @property string $password
 * @property string|null $remember_token
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 * 
 * @property Collection|Administrator[] $administrators
 * @property Collection|Customer[] $customers
 *
 * @package App\Models
 */
class SubMenu extends Model
{
	protected $table = 'submenus';
	protected $primaryKey = 'id';

	protected $fillable = [
		'menu_id',
		'name',
		'icon',
		'url',
		'description',
		'status',
		'no_order',
	];

	public function menu()
	{	
	    return $this->belongsTo("App\Models\Menu", "menu_id");
	}

}
