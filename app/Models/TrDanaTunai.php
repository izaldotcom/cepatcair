<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class TrDanaTunai
 * 
 * @property int $id
 * @property string $nama_lengkap
 * @property string $no_telp
 * @property string $email
 * @property string $alamat
 * @property int $status_bpkp
 * @property string $pemilik_jaminan
 * @property string|null $kode_referal
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 *
 * @package App\Models
 */
class TrDanaTunai extends Model
{
	use SoftDeletes;

	const RUMAH = 1;
	const MOTOR = 2;
	const MOBIL = 3;

	protected $table = 'tr_dana_tunai';
	protected $primaryKey = 'id';

	protected $casts = [
		'status_bpkp' => 'int'
	];

	protected $fillable = [
		'nama_lengkap',
		'no_telp',
		'email',
		'alamat',
		'status_bpkb',
		'tipe',
		'pemilik_jaminan',
		'kode_referal'
	];
}
